package promauto.jroboplc.plugin.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.Environment;
import promauto.jroboplc.core.api.EnvironmentInst;

import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dbscr {
	private final Logger logger = LoggerFactory.getLogger(Dbscr.class);

	private static Pattern patternActstr = Pattern.compile("(if|do)\\s+(.*)", Pattern.DOTALL);
	private static Pattern patternToken = Pattern.compile("(?<=\\{)\\w+(?=\\})");

	private String name;
	private DatabaseModule database;
	private Map<String,String> params = new HashMap<>();
	private Set<String> tokens = new HashSet<>();
	private List<DbscrAction> actions = new ArrayList<>();

	private Environment env;
	
	public Dbscr(DatabaseModule database, String name) {
		this.database = database;
		this.name = name;
		env = EnvironmentInst.get();
	}
	
	protected DatabaseModule getDatabase() {
		return database;
	}
	

	public boolean load(Object conf) {
		Configuration cm = env.getConfiguration();
		
		actions.clear();
		for(Object obj: cm.toList(conf)) {
			Matcher m = patternActstr.matcher(obj.toString());
			if( m.find() ) {
				String acttype = m.group(1);
				DbscrAction da;
				if( acttype.equals("if") )
					da = new DbscrActionIf(); 
				else {
					assert acttype.equals("do"); 
					da = new DbscrActionDo();
				}
				
				if( da.init(this, m.group(2)) ) {
					actions.add(da);
					continue;
				}
			}
			env.printError(logger, "Bad format:", obj.toString());
			return false; 
		}
		
		return true;
	}
	
	
	public Database.ScriptResult execute(Map<String,String> params) {
		this.params = params;
		Database.ScriptResult scriptResult = new Database.ScriptResult();
		scriptResult.success = true;
		scriptResult.executedDoCount = 0;
		scriptResult.positiveIfCount = 0;
		
		try(Statement st = database.getConnection().createStatement()) {
			boolean res = true;
			for(DbscrAction ac: actions) { 
				if( ac.isCondition() ) {
					if( res = ac.execute(st) )
						scriptResult.positiveIfCount++; 	
				} else
					
				if( res ) {
					ac.execute(st);
					scriptResult.executedDoCount++;
				}
			}
			
			if( scriptResult.executedDoCount > 0 ) {
				env.printInfo(logger, database.getName(), "Commiting " + scriptResult.executedDoCount + " actions");
			}

			database.commit();
				
		} catch (Exception e) {
			env.printError(logger, e, database.getName(), name);
			scriptResult.success = false;
			database.rollback();
		}
		
		if( !scriptResult.success  ||  scriptResult.executedDoCount > 0 )
			env.printInfo(logger, 
					database.getName(), 
					scriptResult.success? "Script executed:": "Script failed:", 
					name, 
					params!=null && params.size()>0? params.toString(): "" );

				
		this.params = null;
		return scriptResult;
	}
	

	
	public String injectParameters(String source) {
		
		tokens.clear();
		Matcher m = patternToken.matcher(source);
		while( m.find() )
			tokens.add( m.group() );

		for( String token: tokens)
			if( !token.equals("schema") ) {
				String value = getParam(token);
				source = source.replace('{' + token + '}', value);
			}

		if( tokens.contains("schema") ) {
			String schema = getParam("schema");
			if( !schema.isEmpty() ) {
				source = source.replaceAll("\\{schema\\}(?=\\w)", schema + database.getSchemaDelimiter());
				source = source.replace("{schema}", schema);
			} else
				source = source.replace("{schema}", "");
		}
		
		tokens.clear();
		
		return source;
	}


	private String getParam(String token) {
		if( params == null )
			return "";
		return params.getOrDefault(token, "");

	}
	
	
}
