package promauto.jroboplc.plugin.database;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.utils.Strings;

public class CmdSql extends AbstractCommand {

	private static final int MAX_PRINT_LINES_COUNT = 100;

	private final Logger logger = LoggerFactory.getLogger(CmdSql.class);
	
	@Override
	public String getName() {
		return "sql";
	}

	@Override
	public String getUsage() {
		return "expression";
	}

	@Override
	public String getDescription() {
		return "executes sql expression";
	}

	
	
	@Override
	public String execute(Console console, Module module, String args) {
		DatabaseModule m = (DatabaseModule) module;
		if( !m.connected )
			return "Database " + m.getName() + " is not connected";

		module.postCommand(this, console, module, args);
		return "";
	}

	
	@Override
	public void executePosted(Console console, Module module, String args) {
		
		DatabaseModule m = (DatabaseModule) module;
		
		try(Statement st = m.connection.createStatement()) {
			if( st.execute(args) ) {
				try(ResultSet rs = st.getResultSet()) {
					ResultSetMetaData rsmd = rs.getMetaData();
					int colcnt = rsmd.getColumnCount();
					ArrayList<String[]> data = new ArrayList<>();

					int i;
					String[] rec = new String[colcnt];
					data.add(rec);
					for (i = 0; i < colcnt; ++i)
						rec[i] = rsmd.getColumnName(i + 1);
					int printLinesCount = 0;
					while (rs.next()  &&  printLinesCount++ < MAX_PRINT_LINES_COUNT) {
						data.add(rec = new String[colcnt]);
						for (i = 0; i < colcnt; ++i)
							if ((rec[i] = rs.getString(i + 1)) == null)
								rec[i] = "null";
					}

					int[] lens = new int[colcnt];
					Arrays.fill(lens, 0);
					for (String[] ss : data)
						for (i = 0; i < ss.length; ++i) {
							if (lens[i] < ss[i].length())
								lens[i] = ss[i].length();
						}


					console.print("\n");
					for (String[] ss : data) {
						for (i = 0; i < colcnt; ++i) {
							console.print(ss[i]);
							console.print(Strings.repeat(lens[i] - ss[i].length() + 2, ' '));
						}
						console.print("\n");
					}

					if(rs.next())
						console.print("...Output limit: " + MAX_PRINT_LINES_COUNT + " lines...\n");
					else
						console.print("Total: " + (data.size() - 1) + "\n");
				}
			} else {
				console.print("Updated records: " + st.getUpdateCount() + "\n" );
			}
			
			st.close();
			m.commit();
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, m.getName() );
			m.rollback();
		}
		
	}

	

}
