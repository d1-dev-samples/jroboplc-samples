package promauto.jroboplc.plugin.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.*;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.*;

public class Tabletag {
    private final Logger logger = LoggerFactory.getLogger(Tabletag.class);

    private static enum Access {RW, RO, WO}

    private final DatabaseModule module;

    private Access access;
    private String table;
    private String fldId;
    private String fldValue;
    private String fldName;
    private String where;

    private Map<Integer,TagRW> tags = new HashMap<>();
    private Tag.Type tagtype = Tag.Type.INT;

    private Environment env = EnvironmentInst.get();

    public Tabletag(DatabaseModule module) {
        this.module = module;
    }

    private String sql;



    public boolean load(Object conf) {
        Configuration cm = EnvironmentInst.get().getConfiguration();

        table = cm.get(conf, "table", "");
        fldId = cm.get(conf, "field.id", "id");
        fldName = cm.get(conf, "field.name", fldId);
        fldValue = cm.get(conf, "field.value", "");
        where = cm.get(conf, "where", "");
        try {
            access = Access.valueOf( cm.get(conf, "access", "RW").toUpperCase() );
        } catch (IllegalArgumentException e) {
            env.printError(logger, e, module.getName());
            access = Access.RW;
            return false;
        }

        return true;
    }


    public boolean connect(Statement st) throws SQLException {
        boolean hasChanges = false;

        sql = String.format("select %s, %s, %s from %s",
                fldId, fldName, fldValue, table);
        if( !where.isEmpty() )
            sql += " where " + where;

        Set<Integer> ids = tags.keySet();

        try(ResultSet rs = st.executeQuery(sql)) {
            // get tagtype
            int t = rs.getMetaData().getColumnType(3);
            Tag.Type tagtype_new;
            if (t == Types.SMALLINT || t == Types.INTEGER)
                tagtype_new = Tag.Type.INT;
            else if (t == Types.FLOAT || t == Types.DOUBLE)
                tagtype_new = Tag.Type.DOUBLE;
            else
                tagtype_new = Tag.Type.STRING;


            // remove
            if (tagtype == tagtype_new) {
                Set<Integer> ids_new = new HashSet<>();
                while (rs.next())
                    ids_new.add(rs.getInt(1));

                ids.removeAll(ids_new);
            }
        }

        try(ResultSet rs = st.executeQuery(sql)) {
            for(Integer id: ids) {
                module.getTagTable().remove( tags.get(id) );
                tags.remove(id);
                hasChanges = true;
            }


            // add or update
            TagTable tt = module.getTagTable();
            while( rs.next() ) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String tagname = table + "." + fldValue + "." + name;
                Tag tag = tt.get(tagname);
                if( tag == null )
                    tag = tt.createTagRW(tagtype, tagname);
                tag.setStatus(Tag.Status.Good);

                if( tag instanceof  TagRW ) {
                    Tag tag1 = tags.get(id);
                    if( tag1 != null  &&  !tag.getName().equals( tag1.getName()) ) {
                        module.getTagTable().remove( tag1 );
                        tags.remove(id);
                        tag1 = null;
                    }

                    if( tag1 == null ) {
                        tags.put(id, (TagRW) tag);
                        hasChanges = true;
                    }
                }
            }
        }

        sql = String.format("select %s, %s from %s", fldId, fldValue, table);
        if( !where.isEmpty() )
            sql += " where " + where;

        return hasChanges;
    }


    public boolean execute(Statement st) throws SQLException {

        boolean needReconnect = false;

        if( access == Access.RW  ||  access == Access.WO)
            for(Map.Entry<Integer,TagRW> ent: tags.entrySet())
                if( ent.getValue().hasWriteValue() )
                    writeValue(st, ent.getKey(), ent.getValue());

        if( access == Access.RW  ||  access == Access.RO) {
            int n = 0;
            try (ResultSet rs = st.executeQuery(sql)) {
                while (rs.next()) {
                    int id = rs.getInt(1);
                    TagRW tag = tags.get(id);
                    if (tag == null)
                        needReconnect = true;
                    else {
                        tag.setReadValString(rs.getString(2));
                    }
                    ++n;
                }
            }
            if (n != tags.size())
                needReconnect = true;
        }

        return needReconnect;
    }

    private void writeValue(Statement st, Integer id, TagRW tag) throws SQLException {
        String wrsql = String.format("update %s set %s=%s where %s=%d",
                table,
                fldValue,
                (tagtype == Tag.Type.STRING? "'"+tag.getWriteValString()+"'": tag.getWriteValString()),
                fldId,
                id
            );

        st.executeUpdate(wrsql);
    }


}
