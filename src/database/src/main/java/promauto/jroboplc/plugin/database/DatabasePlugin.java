package promauto.jroboplc.plugin.database;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class DatabasePlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "database";
	private final Logger logger = LoggerFactory.getLogger(DatabasePlugin.class);

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "database connectivity over jdbc";
	}


	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	public Module createDatabaseModule(String name, Object conf) {
		String type = env.getConfiguration().get(conf, "type", "").toLowerCase();

		DatabaseModule m = null;

		switch (type) {
		
		case "firebird":
			m = new FirebirdDatabaseModule(this, name); break;
			
			
		default:
			env.printError(logger, name, "Unknown database type:", type);
		}
		
		if( m!=null  &&  !m.load(conf) )
			return null;
		
    	return m;
	}



}
