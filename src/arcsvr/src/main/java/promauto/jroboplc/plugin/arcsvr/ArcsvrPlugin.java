package promauto.jroboplc.plugin.arcsvr;


import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class ArcsvrPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "arcsvr";
//	private final Logger logger = LoggerFactory.getLogger(ArcsvrPlugin.class);

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "tag value archiver";
	}

//	@Override
//	public Class<?> getModuleClass() {
//		return ArcsvrModule.class;
//	}

	
	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	public Module createDatabaseModule(String name, Object conf) {
		ArcsvrModule m = new ArcsvrModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
