package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.PatternList;
import promauto.jroboplc.core.api.*;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class MesRegex extends MesBase {
    private final Logger logger = LoggerFactory.getLogger(MesRegex.class);

    String tagdescr;
    Pattern tagregex;

    PatternList filters = new PatternList();

    private boolean needInit;
    private boolean phantomsOnly;
    private String placename;


    public MesRegex(MesHolder holder, int idmsg) {
        super(holder, idmsg);
    }

    /*
    arcmes.regex:
      - id:        111
        mestext:   "Hello"
        tagdescr:   "{1} Весы, {2}"
        tagregex:  "W([\d_]+)\.(.*)"

        clb_inp:   red
        clf_inp:   white
        clb_out:   $400080
        clf_out:   white
        clb_ack:   gray
        clf_ack:   black
        eventtype: 3
        eventval:  2
        tags:
          - W.*\.State
          - ~ W004_2_1.*
     */

    public boolean load(Object conf) {
        if( !super.load(conf) )
            return false;

        Configuration cm = env.getConfiguration();
        placename = cm.get(conf, "place", module.getPlaceName() );

        tagdescr = cm.get(conf, "tagdescr", "");
        String stagregex = cm.get(conf, "tagregex", "");
        if( stagregex.isEmpty() )
            tagregex = null;
        else
            tagregex = Pattern.compile(stagregex);


        try {
            filters.load(conf, "tags");
        } catch(PatternSyntaxException e) {
            env.printError(logger, e, module.getName(), "idmsg=" + idmsg, "Load error");
            return false;
        }


        return true;
    }


    @Override
    public void link() {
        recs.values().stream().forEach(rec -> rec.tobeRemoved = true);

        for(Module mod: env.getModuleManager().getModules() ) {

            for (Tag tag : mod.getTagTable().values()) {
                if( tag.getType() == Tag.Type.STRING   ||  tag.getStatus() == Tag.Status.Deleted )
                    continue;

                String tagname = tag.hasFlags(Flags.EXTERNAL)?
                        tag.getName() :
                        mod.getName() + '.' + tag.getName();

                if ( filters.match(tagname) ) {
                    MesRec rec = recs.get(tagname);
                    if (rec == null) {
                        rec = createRec(tagname);
                        needInit = true;
                    }
                    needInit |= rec.phantom;

                    if( rec.tag != tag ) {
                        rec.value = tag.getInt();
                        rec.active = eventfunc.apply( rec.value );
                        rec.descr = makeTagDescr(tagname);
                    }

                    rec.mod = mod;
                    rec.tag = tag;
                    rec.phantom = false;
                    rec.tobeRemoved = false;
                }
            }
        }

        needInit |= recs.values().removeIf(rec -> !rec.phantom  &&  rec.tobeRemoved);

        phantomsOnly = recs.values().stream().allMatch(rec -> rec.phantom);
    }


    private String makeTagDescr(String tagname) {
        if( tagregex == null )
            return tagname;

        Matcher m = tagregex.matcher(tagname);
        if( !m.find() )
            return tagname;

        String result_tagdescr = tagdescr;
        for( int i=1; i<=m.groupCount(); ++i ) {
            result_tagdescr = result_tagdescr.replace("{"+i+"}", m.group(i));
        }

        return result_tagdescr;
    }




    @Override
    protected void initTags(Statement st) throws SQLException {

        Map<String, DescrTagid> tmp = getTaglistMap(st);

        for(Map.Entry<String, MesRec> ent: recs.entrySet()) {
            String tagname = ent.getKey();
            MesRec rec = ent.getValue();
            DescrTagid dti = tmp.get(tagname);
            if( dti == null)
                rec.idtag = createTaglistRec(st, tagname, tagname, rec.placeid);
            else {
                rec.idtag = dti.tagid;
                if( !rec.descr.equals(dti.descr)  ||  rec.placeid != dti.placeid )
                    updateTaglistRec(st, rec.idtag, rec.descr, rec.placeid);
                tmp.remove(tagname);
            }
        }

        for(String tagname: tmp.keySet())
            if ( filters.match(tagname) ) {
                MesRec rec = createRec(tagname);
                rec.idtag = tmp.get(tagname).tagid;
                rec.descr = makeTagDescr(tagname);
                rec.phantom = true;
            } else
                deleteTaglistRec(st, tmp.get(tagname).tagid, tagname);
    }


    private MesRec createRec(String tagname) {
        MesRec rec = new MesRec();
        rec.placename = placename;
        recs.put(tagname, rec);
        return rec;
    }


    @Override
    public boolean execute() throws SQLException {
        if (needInit)
            init();

        if (phantomsOnly)
            return true;

        return super.execute();
    }


}
