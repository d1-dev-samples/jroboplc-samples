package promauto.jroboplc.plugin.arcsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.api.*;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class MesPlain extends MesBase {
    private final Logger logger = LoggerFactory.getLogger(MesPlain.class);

    public MesPlain(MesHolder holder, int idmsg) {
        super(holder, idmsg);
    }

    public boolean load(Object conf) {
        if( !super.load(conf) )
            return false;

        Configuration cm = env.getConfiguration();

        String placename = cm.get(conf, "place", module.getPlaceName() );

        Pattern p1 = Pattern.compile("(.*):(.*);(.*)");
        Pattern p2 = Pattern.compile("(.*)\\.(.*);(.*)");

        try {
            for(Object o: cm.toList( cm.get(conf, "tags"))) {
                String s = o.toString();
                Matcher m = p1.matcher(s);
                if( !m.find()  &&  !(m = p2.matcher(s)).find() ) {
                    env.printError(logger, module.getName(), "idmsg=" + idmsg, "Bad string in tags: " + s);
                    return false;
                }
                MesRec rec = new MesRec();
                String tagname = m.group(1) + '.' + m.group(2);
                rec.descr = m.group(3);
                rec.placename = placename;
                recs.put(tagname, rec);
            }
        } catch(PatternSyntaxException e) {
            env.printError(logger, e, module.getName(), "idmsg=" + idmsg, "Load error");
            return false;
        }


        return true;
    }


    @Override
    public void link() {
        recs.values().stream().forEach(rec -> rec.phantom = true);

        for(Module mod: env.getModuleManager().getModules() ) {
            for (Tag tag : mod.getTagTable().values()) {
                if( tag.getType() == Tag.Type.STRING   ||  tag.getStatus() == Tag.Status.Deleted )
                    continue;

                String tagname = tag.hasFlags(Flags.EXTERNAL)?
                        tag.getName() :
                        mod.getName() + '.' + tag.getName();

                MesRec rec = recs.get(tagname);

                if (rec != null) {
                    if (rec.tag != tag) {
                        rec.value = tag.getInt();
                        rec.active = eventfunc.apply(rec.value);
                    }

                    rec.mod = mod;
                    rec.tag = tag;
                    rec.phantom = false;
                }
            }
        }

        recs.values().stream().filter(rec -> rec.phantom).forEach(rec -> {
            rec.mod = null;
            rec.tag = null;
        });
    }



    @Override
    protected void initTags(Statement st) throws SQLException {

        Map<String, DescrTagid> tmp = getTaglistMap(st);

        for(Map.Entry<String,MesRec> ent: recs.entrySet()) {
            String tagname = ent.getKey();
            MesRec rec = ent.getValue();
            DescrTagid dti = tmp.get(tagname);
            if( dti == null)
                rec.idtag = createTaglistRec(st, tagname, rec.descr, rec.placeid);
            else {
                rec.idtag = dti.tagid;
                if( !rec.descr.equals(dti.descr)  ||  rec.placeid != dti.placeid )
                    updateTaglistRec(st, rec.idtag, rec.descr, rec.placeid);
                tmp.remove(tagname);
            }
        }

        for(String tagname: tmp.keySet())
            deleteTaglistRec(st, tmp.get(tagname).tagid, tagname);

        perfomPostFactum(st);
    }



}
