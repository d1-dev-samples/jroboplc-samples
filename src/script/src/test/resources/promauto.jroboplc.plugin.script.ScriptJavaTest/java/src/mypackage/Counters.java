Tag cnt;
Ref tagref;
Ref ref1;
Ref tag2;

@Override
public boolean load() {
    tagref = createTag("thetag", getArg("prm1", 0));
    ref1 = createRef("MyVar3");
    tag2 = createTag("counter", 0);
    cnt = createVar("cnt", 33);
    return true;
}

@Override
public void execute(){
    tagref.setInt( tagref.getInt() -1);
    ref1.setInt( ref1.getInt() + 1);
    cnt.setInt( cnt.getInt() + 2 );
    tag2.setInt( cnt.getInt() + 10 );
}

