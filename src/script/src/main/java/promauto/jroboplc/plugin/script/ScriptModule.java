package promauto.jroboplc.plugin.script;

import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.tools.JavaCompiler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import parsii.tokenizer.ParseException;
import promauto.jroboplc.core.AbstractModule;
import promauto.jroboplc.core.State;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.TagTable;

public class ScriptModule extends AbstractModule {
	private final Logger logger = LoggerFactory.getLogger(ScriptModule.class);
//	private final static Charset charset = Charset.forName("UTF-8");

	protected List<Action> actions = new LinkedList<>();
	protected Map<String,ActionCalc.Expr> calcexprs = new HashMap<>();
	protected Map<String,Tag> vars = new HashMap<>();
	
	protected Path javaDirSrc;
	protected Path javaDirBin;
	protected JavaCompiler javaCompiler = null;
	protected URLClassLoader javaURLClassLoader = null;
	protected Map<String,Class<?>> javaClasses = new HashMap<>();

	
	public ScriptModule(Plugin plugin, String name) {
		super(plugin, name);
	}

	
	
	public boolean loadModule(Object conf) {
		Configuration cm = env.getConfiguration();
		
		javaDirSrc = Paths.get(cm.get(conf, "java.src", "scripts/java"));
		if(!javaDirSrc.isAbsolute())
			javaDirSrc = cm.getConfDir().resolve(javaDirSrc);

		javaDirBin = Paths.get(cm.get(conf, "java.bin", "scripts/bin"));
		if(!javaDirBin.isAbsolute())
			javaDirBin = cm.getConfDir().resolve(javaDirBin);

		
		// variables
		if( !loadVariables(conf) )
			return false;
		
		// calc expressions
		try {
			ActionCalc.loadExprs( conf, calcexprs );
		} catch (ParseException e) {
			env.printError(logger, e, name, "Error in ActionCalc.loadExpr");
		}
				
		
		// actions
		List<Object> conf_actions = cm.toList( cm.get(conf, "actions") );
		for(Object conf_action: conf_actions) {
			Action sa = createAction( conf_action );
			if( sa == null )
				return false;
			actions.add(sa);
		}

		return true;
	}

	
	



	private boolean loadVariables(Object conf) {
		Configuration cm = env.getConfiguration();
		
		Pattern patvar1 = Pattern.compile("([\\w\\.]+)(?:\\s*;+\\s*(.*:\\w+))?(?:\\s*=\\s*(.*))?");
//		Pattern patvar2 = Pattern.compile("(?:(\\w+):(\\w+)\\s*;*\\s*)");
		Pattern patvar2 = Pattern.compile("(?<=^|;)\\s*(\\w+)\\s*:\\s*(\\w+)\\s*(?=$|;)");
		
		List<Object> conf_var = cm.toList( cm.get(conf, "var") );
		for(Object var: conf_var ) {
			if( var instanceof String ) {
				Matcher m = patvar1.matcher(var.toString());
				if( m.find() ) {
					String varname = m.group(1);
					String varprop = m.group(2);
					String varinit = m.group(3) == null? "": m.group(3);
					String vartype = "int"; 
					boolean varsave = true;
					boolean varhidden = false;
					
					if( varprop != null) {
						m = patvar2.matcher(varprop);
						while( m.find() ) {
							switch( m.group(1) ) {
							case "type":
								vartype = m.group(2); 
								break;
							case "save":
								varsave = m.group(2).equals("on"); 
								break;
							case "hidden":
								varhidden = m.group(2).equals("on"); 
								break;
							}
						}
					}

					Tag tag = tagtable.createTag(vartype, varname, varinit);
					if( tag == null ) {
						env.printError(logger, name, "Var error:", varname, vartype, varinit);
						return false;
					}
					
					if(varsave)
						tag.addFlag(Flags.AUTOSAVE);
					
					if(varhidden)
						tag.addFlag(Flags.HIDDEN);
					
					vars.put(varname, tag);
				}
			} else
			
			// deprecated way (left for backcompatibility)
			if( var instanceof Map ) {
				Map<String,Object> varmap = cm.toMap(var);
				Optional<String> opt = varmap.entrySet().stream()
						.map(ent -> ent.getKey() )
						.filter(tagname -> !tagname.equals("type"))
						.findFirst();
				
				if( opt.isPresent() ) {
					String varname = opt.get();
					String varinit = cm.get(var, varname, "");
					String vartype = cm.get(var, "type", "int"); 
					boolean varsave = cm.get(var, "save", true); 
					
					Tag tag = varsave? 
							tagtable.createTag(vartype, varname, varinit, Flags.AUTOSAVE) :
							tagtable.createTag(vartype, varname, varinit);
							
					if( tag == null ) {
						env.printError(logger, name, "Var error:", varname, vartype, varinit);
						return false;
					}
					vars.put(varname, tag);
				}
			}
			
		}
		
		return true;
	}



	private Action createAction(Object conf) {
		Configuration cm = env.getConfiguration();
		Action sa = null;
		
		Iterator<Map.Entry<String, Object>> it = cm.toMap(conf).entrySet().iterator();
		if( !it.hasNext() )
			return null;
		
		Map.Entry<String, Object> ent = it.next();
		String action_name = ent.getKey();
		Object conf_params = ent.getValue();
		
		
		switch (action_name) {
		case "calc":
			sa = new ActionCalc(this);
			break;
		case "copy":
			sa = new ActionCopy(this);
			break;
		case "java":
			sa = new ActionScriptJava(this);
			break;
		default:
			env.printError(logger, name, "Unknown action:", action_name);
		}
		
		if( sa != null ) {
			if( sa.load(conf_params) )
				return sa;

			env.printError(logger, name, "Action:", action_name, "Id:", sa.getId() );
		}
			
		return null;
	}



	@Override
	public boolean prepareModule() {
		
		for(Action action: actions)
			if( action.isEnabled() )
				if( !action.prepare() ) {
					env.printError(logger, name, "Action:", action.getClass().getSimpleName(), "Id:", action.getId() );
					return false;
				}
		
		return true;
	}

	




	@Override
	public boolean executeModule() {

		for(Action action: actions)
			if( action.isEnabled() )
				action.execute();

		return true;
	}


	
	@Override
	public String getInfo() {
		return enable?
				""
				: "disabled";
	}



	public String getErrors() {
		StringBuilder sb = new StringBuilder();
		for(Action action: actions) {
			if( !action.isEnabled()  ||  action.isValid() )
				continue;
			
			sb.append( action.getError() );
			sb.append( "\r\n" );
		}
		
		return sb.toString();
	}



	public ActionCalc.Expr getCalcExpr(String calcexpr) {
		return calcexprs.get(calcexpr);
	}


	@Override
	protected boolean reload() {
		boolean res = false;
		State state = new State();
		saveState(state);
		ScriptModule tmp = new ScriptModule( plugin, name );

		if( tmp.load() ) {
			transferTagsTo(tmp);

			TagTable oldtagtable = this.tagtable; 
			this.tagtable = tmp.tagtable;
			
			if( tmp.prepare() ) {
				copySettingsFrom(tmp);
				this.actions = tmp.actions;
				this.calcexprs = tmp.calcexprs;
				this.javaClasses = tmp.javaClasses;
				loadState(state);
				res = true;
			} else
				this.tagtable = oldtagtable;
		}

		return res;
	}




	private void transferTagsTo(Module dst) {
		TagTable mytbl = getTagTable();
		TagTable dsttbl = dst.getTagTable();
//		List<Tag> dsttags = new ArrayList<>(dsttbl.getTags());
		
		for(Tag dsttag: dsttbl.getTags("", false)) {
			Tag mytag = mytbl.get(dsttag.getName());
			if( mytag != null ) {
				if( mytag.getType() != dsttag.getType() ) {
					try {
						mytag.copyValueTo(dsttag);
					} catch(NumberFormatException e) {
						env.printError(logger, e, name, mytag.getName() +
								", src: type=" + mytag.getType().name() + " val=" + mytag + 
								", dst: type=" + dsttag.getType().name() + " val=" + dsttag  );
					}
				} else {
					dsttbl.remove(dsttag);
					dsttbl.add(mytag);
					mytag.copyFlagsFrom(dsttag);
				}
			}
		}

		for(Tag mytag: tagtable.getTags("", false))
			if( dsttbl.get(mytag.getName()) == null )
				tagtable.remove(mytag);

	}

	
	@Override
	public void saveState(State state) {
//		Map<String,String> state = new HashMap<String,String>();
		for(Action action: actions)
			action.saveState(state);
//		return state;
	}
	
	
	@Override
	public void loadState(State state) {
		for(Action action: actions)
			action.loadState(state);
//		return true;
	}
	
	

	

}















