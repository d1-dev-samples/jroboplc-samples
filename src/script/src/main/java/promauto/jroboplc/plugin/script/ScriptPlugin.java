package promauto.jroboplc.plugin.script;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class ScriptPlugin extends AbstractPlugin {

	private static final String PLUGIN_NAME = "script";
	
	@Override
	public void initialize() {
		super.initialize();
		env.getCmdDispatcher().addCommand(this, CmdCheck.class);
		env.getCmdDispatcher().addCommand(this, CmdTest.class);
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "copy and calculate tags";
    }
	
//	@Override
//    public Class<?> getModuleClass(){
//    	return ScriptModule.class;
//    }
	

	@Override
	public Module createModule(String name, Object conf) {
		ScriptModule m = new ScriptModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}

}
