package promauto.jroboplc.plugin.script;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.api.ANSI;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.core.api.Plugin;


public class CmdCheck extends AbstractCommand {
	final Logger logger = LoggerFactory.getLogger(CmdCheck.class);

	@Override
	public String getName() {
		return "check";
	}

	@Override
	public String getUsage() {
		return "[mod ...]";
	}

	@Override
	public String getDescription() {
		return "checks and shows errors";
	}

	
	
	@Override
	public String execute(Console console, Plugin plugin, String args) {
		
		Set<Module> mods = EnvironmentInst.get().getModuleManager().getModules();
		
		Pattern pattern = null;
		try {
			if (!args.isEmpty())
				pattern = Pattern.compile(args);
		} catch (PatternSyntaxException e) {
			EnvironmentInst.get().printError(logger, e, "Bad regex:", args);
		}

		int total = 0;
		int badcnt = 0;
		StringBuilder sb = new StringBuilder();
		
		for(Module mod: mods) {
			if( !mod.isEnabled()  ||  !(mod instanceof ScriptModule) )
				continue;
			
			if (pattern!=null)
				if (!pattern.matcher(mod.getName()).matches())
					continue;
			
			total++;

			ScriptModule sm = (ScriptModule)mod;
			
			String errors = sm.getErrors();
			if( errors.isEmpty() )
				continue;

			badcnt++;
			
			sb.append(ANSI.BOLD);
			sb.append(mod.getName());
			sb.append(":\r\n");
			sb.append(ANSI.RESET);
			sb.append(errors);
			sb.append("\r\n");
		}

		sb.append( "Checked modules: ");
		sb.append( ANSI.BOLD );
		sb.append( "" + total + "\r\n");
		sb.append( ANSI.RESET );
		
		if( badcnt > 0 ) {
			sb.append( ANSI.RED );
			sb.append( "Modules with errors: ");
			sb.append( ANSI.BOLD );
			sb.append( "" + badcnt + "\r\n");
			sb.append( ANSI.RESET );
		} else {
			sb.append( "OK!");
		}
		
		return sb.toString();
	}


}
