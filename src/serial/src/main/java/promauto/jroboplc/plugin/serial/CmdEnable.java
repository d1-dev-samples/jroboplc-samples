package promauto.jroboplc.plugin.serial;

public class CmdEnable extends CmdDisable {
	
	public CmdEnable() {
		modeDisable = false;
	}

	@Override
	public String getName() {
		return "enable";
	}

}
