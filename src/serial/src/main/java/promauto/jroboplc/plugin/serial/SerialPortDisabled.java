package promauto.jroboplc.plugin.serial;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;

public class SerialPortDisabled implements SerialPort {

	private volatile boolean valid = true;

	private int id;
	private SerialManagerModule module;

	
	public SerialPortDisabled(SerialManagerModule module) {
		this.module = module;
	}
	
	public boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		id = cm.get(conf, "id",	0);
		module.getTagOpened(id).setReadValInt(0);
		return true;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		return true;
	}

	@Override
	public boolean open() {
		return true;
	}

	@Override
	public void close() {
	}

	@Override
	public boolean isOpened() {
		return false;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public int getAvailable() throws Exception {
		return 0;
	}

	@Override
	public int readByte() throws Exception {
		return -1;
	}

	@Override
	public int readBytes(int[] buff, int size) throws Exception {
		return 0;
	}

	@Override
	public String readString(int size) throws Exception {
		return "";
	}

	@Override
	public String readStringDelim(int delim) throws Exception {
		return "";
	}

	@Override
	public boolean writeByte(int data) throws Exception {
		return false;
	}

	@Override
	public boolean writeBytes(int[] data, int size) throws Exception {
		return false;
	}

	@Override
	public boolean writeString(String data) throws Exception {
		return false;
	}

	@Override
	public boolean discard() throws Exception {
		return false;
	}

	@Override
	public String getInfo() {
		return id + ": disabled";
	}

	@Override
	public int readBytesDelim(int[] buff, int delim) throws Exception {
		return 0;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}


}
