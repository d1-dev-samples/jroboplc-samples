package promauto.jroboplc.plugin.serial;

import java.nio.charset.Charset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;
import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.tags.TagRW;

public class SerialPortJSSC implements SerialPort {
	final Logger logger = LoggerFactory.getLogger(SerialPortJSSC.class);
	private final static Charset charset = Charset.forName("windows-1251");

	private volatile boolean valid = true;

	private static final int BUFFIN_SIZE = 2048;
	private byte[] buffin = new byte[BUFFIN_SIZE];

	private int id;
	private int baud;
	private int databits;
	private int parity; 
	private int stopbits;
	private int timeout_ms;
	private boolean opened = false;

	private jssc.SerialPort port = null;
	private String portname = "";
//	private long bytetime = 0;

	private SerialManagerModule module;
	private TagRW tagOpened;

	public SerialPortJSSC(SerialManagerModule module) {
		this.module = module;
	}

	public synchronized boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id 			= cm.get(conf, "id",		0);
		baud 		= cm.get(conf, "baud", 		9600);
		timeout_ms 	= cm.get(conf, "timeout", 	200);

		tagOpened = module.getTagOpened(id);

		portname = cm.get(conf, "sysname", "");
		port = new jssc.SerialPort( portname );

		String[] bits = cm.get(conf, "bits", "8/0/1").split("/");
		if(bits.length == 3) {
			databits = Integer.parseInt(bits[0]); 
			parity 	 = Integer.parseInt(bits[1]); 
			stopbits = Integer.parseInt(bits[2]);
		} else {
			databits = 8; 
			parity 	 = 0; 
			stopbits = 1;
		}
			
		
		return true;
	}
	
	@Override
	public synchronized boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		boolean reopen = false;
		reopen |= baud != this.baud; 
		reopen |= databits != this.databits; 
		reopen |= parity != this.parity; 
		reopen |= stopbits != this.stopbits;
		reopen &= opened;
		
		this.baud = baud;
		this.databits = databits;
		this.parity = parity;
		this.stopbits = stopbits;
		this.timeout_ms = timeout;
		
		boolean res = true;
		if( reopen ) {
			close();
			res = open();
		}
		return res;
	}

	
	@Override
	public int getId() {
		return id;
	}


	@Override
	public boolean isOpened() {
		return opened;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}

	
	@Override
	public synchronized boolean open() {
		if( opened ) 
			return true;
		
		boolean setup = false;
		try {
			if( !(opened = port.openPort()) ) 
				EnvironmentInst.get().printError(logger, module.getName(), "Port not opened", portname);
			else
				if( !(setup = port.setParams(baud, databits, stopbits, parity)) )
					EnvironmentInst.get().printError(logger, module.getName(), "Error on setParams", portname);
			
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "Open port:", portname);
			opened = false;
		}
		
		if( opened  &&  !setup )
			try {
				port.closePort();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Error on closePort", portname);
			}

//		if( baud > 0 )
//			bytetime  = (long)Math.ceil( (double)(databits + (parity>0? 1: 0) + stopbits) * 1_000_000_000d / (double)baud );

		if( opened )
			tagOpened.setReadValInt(1);

		return opened;
	}

	
	
	@Override
	public synchronized void close() {
		if( !opened ) 
			return;

		try {
			port.closePort();
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "Close port:", portname);
		}
		opened = false;
		
		tagOpened.setReadValInt(0);
	}


	
	@Override
	public int getAvailable() throws Exception {
		return port.getInputBufferBytesCount();
	}


	@Override
	public synchronized int readByte() {
		if( !opened )
			return -1;
		
		try {
			return port.readBytes(1, timeout_ms)[0] & 0xFF;
		} catch (SerialPortException | SerialPortTimeoutException e) {
		}

		return -1;
	}

	
	
	@Override
	public synchronized int readBytes(int[] buff, int size) {
		if( !opened )
			return 0;
		
		try {
			byte[] bufftmp = port.readBytes(size, timeout_ms);
			for (int i=0; i<size; i++)
				buff[i] = bufftmp[i] & 0xFF;
			return size;
		} catch (SerialPortException | SerialPortTimeoutException e) {
		}
		
		return 0;
	}

	
	@Override
	public synchronized int readBytesDelim(int[] buff, int delim) {
		if( !opened )
			return 0;

		long time = System.currentTimeMillis();
		int sizemax = buff.length;
		int size = 0;
		int b;
		
		try {
			while (true) {
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					return -size; // timeout
				} 
				
				b = port.readBytes(1, timeout_ms)[0] & 0xFF;
				if( size < sizemax )
				{
					buff[size++] = b;
					if (b == delim )
						break;
				} else
					return -size; // overflow
			}
		} catch(SerialPortException | SerialPortTimeoutException e) {
			return -size; // timeout
		}
		return size; 
	}

	
	@Override
	public synchronized String readString(int size) {
		if( !opened )
			return "";

		try {
			return port.readString(size, timeout_ms);
		} catch (SerialPortException | SerialPortTimeoutException e) {
		}
		
		return "";
	}
	
	

	
	@Override
	public synchronized String readStringDelim(int delim) {
		
		if( !opened )
			return "";

		long time = System.currentTimeMillis();
		StringBuilder sb = null;
		int i = 0;
		int b = -1;
		try {
			while (true) {
			
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					return ""; // timeout
				} 
				
				b = port.readIntArray(1, timeout_ms)[0];
				buffin[i++] = (byte)(b & 0xFF);
				
				if (b == delim )
					break;
				
				if( i >= BUFFIN_SIZE )
				{
					i = 0;
					if( sb == null )
						sb = new StringBuilder();
					sb.append( new String( Arrays.copyOf(buffin, i), charset) );
				}
			}
		} catch(SerialPortException | SerialPortTimeoutException e) {
		}
		

		if( sb == null )
			return new String( Arrays.copyOf(buffin, i), charset);
		else
		{
			sb.append( new String( Arrays.copyOf(buffin, i) ) );
			return sb.toString();
		}
	}

	
	
	@Override
	public synchronized boolean writeByte(int data) throws Exception {
		if( !opened )
			return false;
		
		return port.writeInt(data);
	}


	@Override
	public synchronized boolean writeBytes(int[] data, int size) throws Exception {
		if( !opened )
			return false;
		
		if( data.length == size)
			return port.writeIntArray(data);
		else
			return port.writeIntArray( Arrays.copyOf(data, size) );
	}


	@Override
	public synchronized boolean writeString(String data) throws Exception {
		if( !opened )
			return false;

		return port.writeString(data);
	}


	@Override
	public synchronized boolean discard() {
		if( !opened )
			return false;
		
		try {
			if (port.getInputBufferBytesCount() > 0) 
				port.readIntArray();
			
			return true;

		} catch(SerialPortException e) {
		}
		return false;
	}


	@Override
	public String getInfo() {
		return String.format("%d: jssc  %s %d %d/%d/%d %d %s", 
			id,
			portname,
			baud,
			databits,
			parity,
			stopbits,
			timeout_ms,
			(opened? "opened": "closed"));
	}


}
