package promauto.jroboplc.plugin.serial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class SerialPlugin extends AbstractPlugin {
	private final Logger logger = LoggerFactory.getLogger(SerialPlugin.class);

	private static final String PLUGIN_NAME = "serial";

	
	@Override
	public void initialize() {
		super.initialize();
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Serial port support";
    }
	

	@Override
	public Module createModule(String name, Object conf) {
    	if( env.getSerialManager() != null ) {
			env.printError(logger, name, "is ignored. Serial manager is already installed.");
    		return null;
    	}

    	SerialManagerModule m = new SerialManagerModule(this, name);
    	env.setSerialManager(m);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}


}
