package promauto.jroboplc.plugin.serial;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.SerialPort;
import promauto.jroboplc.core.tags.TagRW;

public class SerialPortNPort implements SerialPort {
	final Logger logger = LoggerFactory.getLogger(SerialPortNPort.class);
	private final static Charset charset = Charset.forName("windows-1251");

	private Socket socketData;
	private Socket socketCmd;
	
	private InputStream inputData;
	private OutputStream outputData;
	private InputStream inputCmd;
	private OutputStream outputCmd;
	
	private volatile boolean valid = true;
	private volatile boolean opened = false;

	private String host;
	private int portData;
	private int portCmd;
	private InetSocketAddress addrData;
	private InetSocketAddress addrCmd;
	
	private static final int BUFFIN_SIZE = 2048;
	private byte[] buffin = new byte[BUFFIN_SIZE];
	private byte[] buffcmd = new byte[10];

	private int id;
	
	private int baud;
	private int databits;
	private int parity; 
	private int stopbits;

	private byte baudCode;
	private byte modeCode;
	
	private int timeout_ms;
	private boolean noTcpDelay;
//	private long timeout_ns;

	private boolean connectionLost = false;
	private long connectionLostTime = 0;
	private long recon_ms = 3000;

	private SerialManagerModule module;
	private TagRW tagOpened;


	
	public SerialPortNPort(SerialManagerModule module) {
		this.module = module;
	}

	public synchronized boolean load(Object conf) {
		Configuration cm = EnvironmentInst.get().getConfiguration();
		
		id 			= cm.get(conf, "id",			0);
		baud 		= cm.get(conf, "baud", 			9600);
		recon_ms 	= cm.get(conf, "recon_ms", 		3000);
		timeout_ms 	= cm.get(conf, "timeout", 		200);
		noTcpDelay 	= cm.get(conf, "no_tcpdelay",	true);
		
		tagOpened = module.getTagOpened(id);

		host = cm.get(conf, "host", "");
		portData = cm.get(conf, "port.data", 0);
		portCmd = cm.get(conf, "port.cmd", 0);
		addrData = new InetSocketAddress(host, portData);
		addrCmd = new InetSocketAddress(host, portCmd);

		String[] bits = cm.get(conf, "bits", "8/0/1").split("/");
		databits = bits.length > 0? Integer.parseInt(bits[0]): 8; 
		parity 	 = bits.length > 1? Integer.parseInt(bits[1]): 0; 
		stopbits = bits.length > 2? Integer.parseInt(bits[2]): 1;
		
		baudCode = getBaudCode( baud );
		modeCode = getModeCode( databits, parity, stopbits );
		if( baudCode < 0  ||  modeCode < 0 )
			return false;
		
		return true;
	}
	
	
	private byte getBaudCode(int baud) {
		switch( baud ) {
		case 300:	 return 0;
		case 600:	 return 1;
		case 1200:	 return 2;
		case 2400:	 return 3;
		case 4800:	 return 4;
		case 7200:	 return 5;
		case 9600:	 return 6;
		case 19200:	 return 7;
		case 38400:	 return 8;
		case 57600:	 return 9;
		case 115200: return 10;
		case 230400: return 11;
		case 150:	 return 14;
		case 134:	 return 15;
		case 110:	 return 16;
		case 75:	 return 17;
		case 50:	 return 18;
		}
		EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Baud:", ""+baud);
		return -1;
	}
	
	private byte getModeCode(int databits, int parity, int stops) {
		byte mode;
		
		switch( databits ) {
		case 8: mode = 3; break; 
		case 7: mode = 2; break; 
		case 6: mode = 1; break; 
		case 5: mode = 0; break;
		default:
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Databits:", ""+databits);
			return -1;
		}
		
		switch( parity ) {
		case 0: mode |= 0; break; 
		case 1: mode |= 16; break; 
		case 2: mode |= 8; break; 
		case 3: mode |= 24; break;
		case 4: mode |= 32; break;
		default:
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Parity:", ""+parity);
			return -1;
		}

		switch( stops ) {
		case 1: mode |= 0; break; 
		case 2: mode |= 4; break; 
		default:
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Stopbits:", ""+stops);
			return -1;
		}

		return mode;
	}

	
	
	@Override
	public int getId() {
		return id;
	}

	@Override
	public synchronized boolean setParams(int baud, int databits, int parity, int stopbits, int timeout) {
		boolean reopen = false;
		reopen |= baud != this.baud; 
		reopen |= databits != this.databits; 
		reopen |= parity != this.parity; 
		reopen |= stopbits != this.stopbits;
		reopen &= opened;
		
		byte baudCode = getBaudCode( baud );
		byte modeCode = getModeCode( databits, parity, stopbits );
		if( baudCode < 0  ||  modeCode < 0 )
			return false;
		
		this.baud = baud;
		this.databits = databits;
		this.parity = parity;
		this.stopbits = stopbits;
		this.timeout_ms = timeout;
		this.baudCode = baudCode;
		this.modeCode = modeCode;

		boolean res = true;
		if( reopen ) {
			close();
			res = open();
		}
		return res;
	}

	@Override
	public synchronized boolean open() {
		return open(false);
	}

	public synchronized boolean open(boolean silent) {
		if( opened ) 
			return true;
		
		opened = connectSockets(silent)  &&  applyParams();
		if( !opened ) {
			connectionLost = true;
			disconnectSockets();
		} else {
			connectionLost = false;
			tagOpened.setReadValInt(1);
		}
		
		return opened;
	}
	
	
	
	protected boolean connectSockets(boolean silent) {
		try {
			socketCmd = null;
			socketData = null;
			
			socketData = new Socket();
			socketData.setSoTimeout(timeout_ms);
			socketData.setTcpNoDelay(noTcpDelay);
			socketData.connect( addrData, timeout_ms );
			inputData = socketData.getInputStream();
			outputData = socketData.getOutputStream();
			
			socketCmd = new Socket();
			socketCmd.setSoTimeout(timeout_ms);
			socketCmd.connect( addrCmd, timeout_ms );
			inputCmd = socketCmd.getInputStream();
			outputCmd = socketCmd.getOutputStream();
		} catch (Exception e) {
			if( !silent )
				EnvironmentInst.get().printError(logger, e, module.getName(), "Connect sockets:", getDescr());
			return false;
		}
		return true;
	}

	
	private void disconnectSockets() {
		if( socketCmd != null ) {
			try {
				socketCmd.close();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Disconnect sockets:", getDescr(), "socketCmd");
			}
			socketCmd = null;
		}
		
		if( socketData != null ) {
			try {
				socketData.close();
			} catch (Exception e) {
				EnvironmentInst.get().printError(logger, e, module.getName(), "Disconnect sockets", getDescr(), "socketData");
			}
			socketData = null;
		}
		
		opened = false;
		tagOpened.setReadValInt(0);
	}

	
	private boolean applyParams() {
		try {
			// D_COMMAND_FLOWCTRL
			buffcmd[0] = 0x11;
			buffcmd[1] = 4;
			buffcmd[2] = 0;
			buffcmd[3] = 0;
			buffcmd[4] = 0;
			buffcmd[5] = 0;
			sendCommand(6, 3); 

			// D_COMMAND_IOCTL
			buffcmd[0] = 0x10;
			buffcmd[1] = 2;
			buffcmd[2] = baudCode;
			buffcmd[3] = modeCode;
			sendCommand(4, 3); 

			// D_COMMAND_LINECTRL
			buffcmd[0] = 0x12;
			buffcmd[1] = 2;
			buffcmd[2] = 1;
			buffcmd[3] = 1;
			sendCommand(4, 3); 

			// D_COMMAND_TX_FIFO
			buffcmd[0] = 0x30;
			buffcmd[1] = 1;
			buffcmd[2] = 0x10;
			sendCommand(3, 3); 

		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "Apply params:", getDescr());
			return false;
		}
		return true;
	}

	
	@Override
	public synchronized void close() {
		if( !opened ) 
			return;

		try {
			// D_COMMAND_WAIT_OQUEUE
			buffcmd[0] = 0x2f;
			buffcmd[1] = 4;
			buffcmd[2] = 0x4c;
			buffcmd[3] = 0x1d;
			buffcmd[4] = 0;
			buffcmd[5] = 0;
			sendCommand(6, 4); 
	
			// D_COMMAND_FLUSH
			buffcmd[0] = 0x14;
			buffcmd[1] = 1;
			buffcmd[2] = 2;
			sendCommand(3, 3);
			
		} catch (Exception e) {
			EnvironmentInst.get().printError(logger, e, module.getName(), "Close port:", getDescr());
		}

		disconnectSockets();
//		opened = false;
		connectionLost = false;
		tagOpened.setReadValInt(0);
	}

	
	private void checkLiveness() {
		try {
	
			// D_COMMAND_LSTATUS
			byte cmd = 0x13;
			buffcmd[0] = cmd;
			buffcmd[1] = 0;
			sendCommand(2, 5);
			return;
		
		} catch (Exception e) {
		}
		
		if( opened ) {
			EnvironmentInst.get().printError(logger, module.getName(), getDescr(), "Connection lost");
			disconnectSockets();
			connectionLost  = true;
			connectionLostTime = System.currentTimeMillis();
		}
	}


	
	private String getDescr() {
		return "Serial port " + id + " (NPort " + host + ":" + portData + "/" + portCmd + ")";
	}

	private boolean restoreConnectionIfLost() {
		if( !connectionLost )
			return true;
		
		if( System.currentTimeMillis() - connectionLostTime < recon_ms )
			return false;
		
		if( open(true) ) {
			EnvironmentInst.get().printInfo(logger, getDescr(), "Connection restored");
			return true;
		}
		
		connectionLostTime = System.currentTimeMillis();
		return false;
	}

	
	private void sendCommand(int reqlen, int resplen) throws IOException {
		
		int cmd = buffcmd[0];
		outputCmd.write(buffcmd, 0, reqlen);

		while(true) {
			int b = inputCmd.read();
			if( b == 0x26 ) {
				if( inputCmd.skip(3) == 3 )
					continue;
				else
					throw new IOException("Got bad notification, cmd: " + cmd);
			}
			
			if( b == cmd )
				if( inputCmd.skip(resplen-1) == resplen - 1 )
					break;
			
			throw new IOException("Bad answer, cmd: " + cmd);
		}
	}



	@Override
	public boolean isOpened() {
		return opened;
	}

	@Override
	public boolean isValid() {
		return valid;
	}

	@Override
	public void setInvalid() {
		valid = false;
	}

	
	
	@Override
	public int getAvailable() throws IOException {
		return inputData.available();
	}


	
	@Override
	public synchronized int readByte() {
		if( !restoreConnectionIfLost()  ||  !opened )
			return -1;
		
		try {
			return inputData.read();
		} catch(IOException e) {
		}

		checkLiveness();
		return -1;
	}

	
	@Override
	public synchronized int readBytes(int[] buff, int size) {
		if( !restoreConnectionIfLost() )
			return 0;
		
		if( !opened )
			return 0;
		
		long time = System.currentTimeMillis();
		int n = 0;
		byte[] bufftmp = new byte[size];
		try {
			int p = 0;
			while(true) {
				n += inputData.read(bufftmp, p, size-p);
				
				if( n < size ) {
					if ( (System.currentTimeMillis() - time) >= timeout_ms) 
						break;
					p = n;
				} else
					break;
			}
		} catch(IOException e) {
		}

		if( n > size )
			n = size;
		
		for (int i=0; i<n; i++)
			buff[i] = bufftmp[i] & 0xFF;
		
		if( n==size )
			return n;
	
		checkLiveness();
		return -n;
	}

	
	
	@Override
	public synchronized int readBytesDelim(int[] buff, int delim) {
		if( !restoreConnectionIfLost() )
			return 0;
		
		if( !opened )
			return 0;

		long time = System.currentTimeMillis();
		int sizemax = buff.length;
		int size = 0;
		int b;
		
		try {
			while (true) {
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					checkLiveness();
					return -size; // timeout
				} 
				
				b = inputData.read();
				if( size < sizemax )
				{
					buff[size++] = b;
					if (b == delim )
						break;
				} else
					return -size; // overflow
			}
		} catch(IOException e) {
			checkLiveness();
			return -size; // timeout
		}
		return size; 
	}


	
	@Override
	public synchronized String readString(int size) {
		if( !restoreConnectionIfLost()  ||  !opened )
			return "";

		long time = System.currentTimeMillis();
		int n = 0;
		try {
			byte[] bufftmp = new byte[size];
			int p = 0;
			while(true) {
				n += inputData.read(bufftmp, p, size-p);
				
				if( n != size ) {
					if ( (System.currentTimeMillis() - time) >= timeout_ms) {
						checkLiveness();
						return ""; // timeout
					} 
					p = n;
				} else
					break;
					
			}
			
			if( n==size )
				return new String(bufftmp, charset);
		
		} catch(IOException e) {
		}

		checkLiveness();
		return "";

	}

	
	@Override
	public synchronized String readStringDelim(int delim) {
		if( !restoreConnectionIfLost()  ||  !opened )
			return "";

		long time = System.currentTimeMillis();
		StringBuilder sb = null;
		int i = 0;
		int b = -1;
		try {
			while (true) {
			
				if ( (System.currentTimeMillis() - time) >= timeout_ms) {
					checkLiveness();
					return ""; // timeout
				} 
				
				b = inputData.read();
				buffin[i++] = (byte)b;
				
				if (b == delim )
					break;
				
				if( i >= BUFFIN_SIZE )
				{
					i = 0;
					if( sb == null )
						sb = new StringBuilder();
					sb.append( new String( Arrays.copyOf(buffin, i), charset) );
				}
			}
		} catch(IOException e) {
		}
		
		if( b != delim )
			checkLiveness();

		if( sb == null )
			return new String( Arrays.copyOf(buffin, i), charset );
		else
		{
			sb.append( new String( Arrays.copyOf(buffin, i), charset ) );
			return sb.toString();
		}
	}

	
	@Override
	public synchronized boolean writeByte(int data) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		try {
			outputData.write(data);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		
		return true;
	}

	
	@Override
	public synchronized boolean writeBytes(int[] data, int size) {
		if( !restoreConnectionIfLost() )
			return false;
		
		if( !opened )
			return false;
		
		byte[] bufftmp = new byte[size];
		for(int i=0; i<size; ++i)
			bufftmp[i] = (byte)data[i];
		
		try {
			outputData.write(bufftmp);
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		return true;
	}

	
	@Override
	public synchronized boolean writeString(String data) {
		if( !restoreConnectionIfLost() )
			return false;

		if( !opened )
			return false;
		
		try {
			outputData.write( data.getBytes(charset) );
		} catch (IOException e) {
			checkLiveness();
			return false;
		}
		return true;
	}

	

	@Override
	public synchronized boolean discard() {
		if( !opened )
			return false;
		
		try {
			if( inputData.available() > 0 )
				inputData.skip( inputData.available() );
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	
	@Override
	public String getInfo() {
		return String.format("%d: nport %s:%d %d %d/%d/%d %d %s",
				id,
				host,
				portData,
				baud,
				databits,
				parity,
				stopbits,
				timeout_ms,
				(opened? "opened": "closed"));
	}

}
