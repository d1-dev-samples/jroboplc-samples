package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;

public class OwenTrm210Module extends PeripherialModule {
	
	private final Logger logger = LoggerFactory.getLogger(OwenTrm210Module.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected Tag tagLvoPSTAT; 
    protected Tag tagLvoPPV;   
    protected TagRW tagLvoPSP;
    protected Tag tagLvoPSETP;
    protected Tag tagLvoPO;  
    protected TagRW tagLvoPrL;
    protected TagRW tagLvoProut;
    protected TagRW tagLvoPRS;
    protected TagRW tagLvoPAT;
    protected TagRW tagOLL;
    protected TagRW tagOLH;
    protected TagRW tagSLL;
    protected TagRW tagSLH;
    protected TagRW tagorEU;
    protected TagRW tagCntL;


	
	public OwenTrm210Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagLvoPSTAT = 								tagtable.createInt("LvoP.STAT", 0, Flags.STATUS);
		tagLvoPPV	= 								tagtable.createInt("LvoP.PV", 0, Flags.STATUS);
		tagLvoPSP	= protocol.addWriteTag( 0x0002, tagtable.createRWInt("LvoP.SP", 0, Flags.STATUS));
		tagLvoPSETP	= 								tagtable.createInt("LvoP.SET.P", 0, Flags.STATUS);
		tagLvoPO	= 								tagtable.createInt("LvoP.O", 0, Flags.STATUS);
		tagLvoPrL	= protocol.addWriteTag( 0x0005, tagtable.createRWInt("LvoP.r-L", 0, Flags.STATUS));
		tagLvoProut	= protocol.addWriteTag( 0x0006, tagtable.createRWInt("LvoP.r.out", 0, Flags.STATUS));
		tagLvoPRS	= protocol.addWriteTag( 0x0007, tagtable.createRWInt("LvoP.R-S", 0, Flags.STATUS));
		tagLvoPAT	= protocol.addWriteTag( 0x0008, tagtable.createRWInt("LvoP.AT", 0, Flags.STATUS));
		
		tagSLL		= protocol.addWriteTag( 0x0300, tagtable.createRWInt("SL-L", 0, Flags.STATUS));
		tagSLH		= protocol.addWriteTag( 0x0301, tagtable.createRWInt("SL-H", 0, Flags.STATUS));
		tagorEU		= protocol.addWriteTag( 0x0302, tagtable.createRWInt("orEU", 0, Flags.STATUS));
		tagCntL		= protocol.addWriteTag( 0x0303, tagtable.createRWInt("CntL", 0, Flags.STATUS));
		
		tagOLL		= protocol.addWriteTag( 0x030B, tagtable.createRWInt("OL-L", 0, Flags.STATUS));
		tagOLH		= protocol.addWriteTag( 0x030C, tagtable.createRWInt("OL-H", 0, Flags.STATUS));

		return true;
	}

	@Override
	protected void initChannelMap(List<String> chtags) {
		addChannelMapTag(chtags, tagLvoPPV, "0");
		addChannelMapTag(chtags, tagLvoPSP, "sp0");
	}

	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) {
		    tagLvoPSP.acceptWriteValue();   
		    tagLvoPrL.acceptWriteValue();
		    tagLvoProut.acceptWriteValue();
		    tagLvoPRS.acceptWriteValue();
		    tagLvoPAT.acceptWriteValue();   
		    tagOLL.acceptWriteValue();     
		    tagOLH.acceptWriteValue();      
		    tagSLL.acceptWriteValue();      
		    tagSLH.acceptWriteValue();      
		    tagorEU.acceptWriteValue();
		    tagCntL.acceptWriteValue();      
			return true;
		}
		
		boolean result = true;
		
		try {
			result = protocol.sendWriteTags(16);
			
			if( result )
				if( result = protocol.requestCmd3(0, 9) ) {
					tagLvoPSTAT	.setInt(		protocol.getAnswerWord(0) ); 
					tagLvoPPV	.setInt(		protocol.getAnswerWord(1) );
					tagLvoPSP	.setReadValInt(	protocol.getAnswerWord(2) );
					tagLvoPSETP	.setInt(		protocol.getAnswerWord(3) );
					tagLvoPO	.setInt(		protocol.getAnswerWord(4) );
					tagLvoPrL	.setReadValInt(	protocol.getAnswerWord(5) );
					tagLvoProut	.setReadValInt(	protocol.getAnswerWord(6) );
					tagLvoPRS	.setReadValInt(	protocol.getAnswerWord(7) );
					tagLvoPAT	.setReadValInt(	protocol.getAnswerWord(8) );
				}
				            
			if( result )
				if( result = protocol.requestCmd3(0x0300, 4) ) {
					tagSLL		.setReadValInt(	protocol.getAnswerWord(0)  );		
					tagSLH		.setReadValInt(	protocol.getAnswerWord(1)  );
					tagorEU		.setReadValInt(	protocol.getAnswerWord(2)  );
					tagCntL		.setReadValInt(	protocol.getAnswerWord(3)  );
				}
				            
			if( result )
				if( result = protocol.requestCmd3(0x030B, 2) ) {
					tagOLL		.setReadValInt(	protocol.getAnswerWord(0) );
					tagOLH		.setReadValInt(	protocol.getAnswerWord(1) );
				}
			
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}


}
