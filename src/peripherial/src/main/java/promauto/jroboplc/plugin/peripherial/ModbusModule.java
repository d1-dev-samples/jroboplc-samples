package promauto.jroboplc.plugin.peripherial;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Configuration;
import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.*;
import promauto.utils.Numbers;

public class ModbusModule extends PeripherialModule {
	
	private final Logger logger = LoggerFactory.getLogger(ModbusModule.class);

	protected ProtocolModbus protocol = new ProtocolModbus(this);
	
	protected enum ModbusTagType { BOOL, INT16, UINT16, INT32, UINT32, FLOAT16, FLOAT32, STRING }
	
	protected enum ModbusTagAccess { RO, WO, RW }
	
	protected enum ModbusTagRegion { COIL, DSCINP, HLDREG, INPREG }
		
	protected static class ModbusTag {
		public String name;
	    public ModbusTagType type;
		public boolean inverted;
	    public int size;
	    public ModbusTagRegion region;
	    public ModbusTagAccess access;
	    public int address;
	    public boolean littleEndian;
	    public boolean readEnd;
	    public boolean writeSingle;
	    public boolean writeMultiple;
	    public boolean enable;
	    public TagRW tag;
	    
	    public boolean needWrite;

		public String tracktagName;
		public ModbusTag tracktag;

	    
	    
	    public void init() {
	    	
			if( region == ModbusTagRegion.DSCINP  ||  region == ModbusTagRegion.INPREG  ||  type == ModbusTagType.STRING  ) {
				access = ModbusTagAccess.RO;
			}
	    	
			if( region == ModbusTagRegion.COIL  ||  region == ModbusTagRegion.DSCINP ) {
				type = ModbusTagType.BOOL;
				size = 1;
			} else
				switch( type ) {
				case BOOL: case INT16: case UINT16: case FLOAT16: 
					size = 1; break;
				case INT32: case UINT32: case FLOAT32:
					size = 2; break;
				case STRING:
					size = Math.max(size, 1);
				}
	    
			switch( type ) {
			case BOOL: 
				tag = new TagRWBool(name, false, Flags.STATUS); break;
			case INT16: case UINT16: case INT32:
				tag = new TagRWInt(name, 0, Flags.STATUS); break;
			case UINT32:
				tag = new TagRWLong(name, 0, Flags.STATUS); break;
			case FLOAT16: case FLOAT32:
				tag = new TagRWDouble(name, 0.0, Flags.STATUS);	break;
			case STRING:
				tag = new TagRWString(name, "", Flags.STATUS);
			}
			
			
	    }

		public void putValueIntoBuff(int[] buff, int pos) {
			switch( type ) {
			case BOOL:
				buff[pos] = (tag.getWriteValBool() ^ inverted)? 1: 0;
				break;
			case INT16: case UINT16: 
				buff[pos] = tag.getWriteValInt() & 0xFFFF;
				break;
			case INT32: case UINT32: {
				long l = tag.getWriteValInt() & 0xFFFF_FFFFL;
				if( littleEndian) {
					buff[pos + 1] = (int) (l >> 16);
					buff[pos] = (int) (l & 0xFFFF);
				} else {
					buff[pos] = (int) (l >> 16);
					buff[pos + 1] = (int) (l & 0xFFFF);
				}
				break;
			}
			case FLOAT16:
				buff[pos] = Numbers.encodeFloat16( (float)(tag.getWriteValDouble()) );
				break;
			case FLOAT32: {
				long l = Numbers.encodeFloat32((float)(tag.getWriteValDouble())) & 0xFFFF_FFFFL;
				if( !littleEndian ) {
					buff[pos] = (int) (l >> 16);
					buff[pos + 1] = (int) (l & 0xFFFF);
				} else {
					buff[pos + 1] = (int) (l >> 16);
					buff[pos] = (int) (l & 0xFFFF);
				}
				break;
			}
			case STRING:
				// not implemented
			}
		}

		public void fetchValueFromProtocolBuffin(ProtocolModbus protocol, int pos) {
			switch( type ) {
			case BOOL:
				tag.setReadValBool( (protocol.getAnswerWord(pos) != 0) ^ inverted );
				break;
			case INT16: 
				tag.setReadValInt( (short)(protocol.getAnswerWord(pos)) );
				break;
			case UINT16: 
				tag.setReadValInt( protocol.getAnswerWord(pos) );
				break;
			case INT32: 
				tag.setReadValInt( protocol.getAnswerInt32(pos, littleEndian) );
				break;
			case UINT32:
				tag.setReadValLong( protocol.getAnswerInt32(pos, littleEndian) & 0xFFFF_FFFFL );
				break;
			case FLOAT16:
				tag.setReadValDouble( Numbers.decodeFloat16(protocol.getAnswerWord(pos)));
				break;
			case FLOAT32:
				tag.setReadValDouble( Numbers.decodeFloat32(protocol.getAnswerInt32(pos, littleEndian)));
				break;
			case STRING:
				tag.setReadValString( protocol.getAnswerString(pos, size) );
			}
		}
		
	}
	

	protected int maxWriteSizeCoil;
	protected int maxWriteSizeReg;
    
	protected boolean hasReadCoils;
	protected boolean hasReadDscinps;
	protected boolean hasReadHldregs;
	protected boolean hasReadInpregs;
	
	protected List<ModbusTag> modbustags = new ArrayList<>();
	
	private int[] buff = new int[2];

	
	
	public ModbusModule(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {
		modbustags.clear();
		
		Configuration cm = env.getConfiguration();

		String cs = cm.get(conf, "charset", "");
		if( !cs.isEmpty() )
			protocol.charset = Charset.forName(cs);

		maxWriteSizeCoil = cm.get(conf, "maxWriteSizeInp", 128);
		maxWriteSizeReg = cm.get(conf, "maxWriteSizeReg", 16);

		try {
		for(Object conf_tag: cm.toList( cm.get(conf, "tags"))) {
			ModbusTag mtag = new ModbusTag();
			mtag.name = cm.get(conf_tag, "name", "");
			mtag.type = ModbusTagType.valueOf( cm.get(conf_tag, "type", "uint16").toUpperCase() );
			mtag.inverted = cm.get(conf_tag, "inverted", false);
			mtag.region = ModbusTagRegion.valueOf( cm.get(conf_tag, "region", "hldreg").toUpperCase() );
			mtag.access = ModbusTagAccess.valueOf( cm.get(conf_tag, "access", "rw").toUpperCase() );
			mtag.address = cm.get(conf_tag, "address", 0);
			mtag.littleEndian = cm.get(conf_tag, "littleEndian", false);
			mtag.readEnd = cm.get(conf_tag, "readEnd", false);
			mtag.writeSingle = cm.get(conf_tag, "writeSingle", true);
			mtag.writeMultiple = cm.get(conf_tag, "writeMultiple", true);
			mtag.enable = cm.get(conf_tag, "enable", true);
			mtag.size = cm.get(conf_tag, "size", 1);
			mtag.tracktagName = cm.get(conf_tag, "tracktag", "");

			mtag.init();
			tagtable.add(mtag.tag);
			
			modbustags.add(mtag);
		}
		} catch (IllegalArgumentException e) {
			env.printError(logger, e, name);
			return false;
		}
		
		init();

		return true;
	}


	
	private void init() {
		modbustags.sort((a, b) -> a.address - b.address);

		hasReadCoils = false;
		hasReadDscinps = false;
		hasReadHldregs = false;
		hasReadInpregs = false;
		for( ModbusTag mtag: modbustags) {
			if( mtag.access == ModbusTagAccess.WO )
				continue;
			hasReadCoils   |= mtag.enable  &&  mtag.region == ModbusTagRegion.COIL;
			hasReadDscinps |= mtag.enable  &&  mtag.region == ModbusTagRegion.DSCINP;
			hasReadHldregs |= mtag.enable  &&  mtag.region == ModbusTagRegion.HLDREG;
			hasReadInpregs |= mtag.enable  &&  mtag.region == ModbusTagRegion.INPREG;
		}

		Map<String, ModbusTag> map = modbustags.stream().collect(Collectors.toMap(c -> c.name, c -> c));
		for(ModbusTag mt: modbustags)
			if( !mt.tracktagName.isEmpty() ) {
				mt.tracktag = map.get(mt.tracktagName);
			}

	}


	@Override
	public boolean executePeripherialModule() {
		if(emulated) {
			for( ModbusTag mtag: modbustags)
				mtag.tag.acceptWriteValue();
			return true;
		}


		boolean needWriteReg = false;
		boolean needWriteCoil = false;
		for( ModbusTag mtag: modbustags) {
			if( mtag.access == ModbusTagAccess.RO )
				continue;

			mtag.needWrite = mtag.enable  &&
					(mtag.tag.hasWriteValue()  ||
							(mtag.tracktag != null  &&  !mtag.tag.equalsValue(mtag.tracktag.tag)));

			needWriteCoil |= mtag.needWrite  &&  (mtag.region == ModbusTagRegion.COIL);
			needWriteReg  |= mtag.needWrite  &&  (mtag.region == ModbusTagRegion.HLDREG);
		}

		if( needWriteReg ) {
			if( !writeMultiple(ModbusTagRegion.HLDREG) )
				return false;
	
			if( !writeSingle(ModbusTagRegion.HLDREG) )
				return false;
		}
			
		if( needWriteCoil ) {
			if( !writeMultiple(ModbusTagRegion.COIL) ) 
				return false;
			
			if( !writeSingle(ModbusTagRegion.COIL) )
				return false;
		}
		
		if( hasReadHldregs  &&  !readRegisters(ModbusTagRegion.HLDREG) )
			return false;
		
		if( hasReadInpregs  &&  !readRegisters(ModbusTagRegion.INPREG) )
			return false;
		
		if( hasReadCoils  &&  !readRegisters(ModbusTagRegion.COIL) )
			return false;
		
		if( hasReadDscinps  &&  !readRegisters(ModbusTagRegion.DSCINP) )
			return false;
		
		
	
		return true;
	}




	private boolean writeMultiple(ModbusTagRegion region) {
		int maxWriteSize = (region == ModbusTagRegion.HLDREG)? maxWriteSizeReg: maxWriteSizeCoil;
		if( maxWriteSize <= 0 )
			return true;
		
		int beg = 0;
		int size = 0;
		int buffsize = 0;
		int nextaddr = 0;

		ModbusTag mtag;
		for(int i=0; i<modbustags.size(); ++i) {
			mtag = modbustags.get(i);
			
			if( size >= maxWriteSize  ||  (size > 0  &&  mtag.address != nextaddr) ) {
				if( !writeMultiple1(region, beg, size, buffsize) )
					return false;
				size = 0;
				buffsize = 0;
			}
			
			if( mtag.needWrite  &&  
				mtag.writeMultiple  &&  
				mtag.region == region )
			{
				if( size == 0 )
					beg = i;
				size++;
				buffsize += mtag.size;
				nextaddr = mtag.address + mtag.size; 
			} 
		}
		
		if( size > 0 )
			if( !writeMultiple1(region, beg, size, buffsize) )
				return false;
		
		return true;
	}


	private boolean writeMultiple1(ModbusTagRegion region, int beg, int size, int buffsize) {
		if( size == 0  ||  (buffsize == 1  &&  modbustags.get(beg).writeSingle) )
			return true;
		
		adjustBuff(buffsize);
		int pbuff = 0;
		int end = beg + size;

		ModbusTag mtag;
		for(int j=beg; j<end; ++j) {
			mtag = modbustags.get(j);
			mtag.putValueIntoBuff(buff, pbuff);
			if( mtag.access == ModbusTagAccess.WO )
				mtag.tag.copyLastWriteToRead();
			mtag.needWrite = false;
			pbuff += mtag.size;
		}
		
		boolean result = false;
		try {
			result = (region == ModbusTagRegion.HLDREG)?
					protocol.requestCmd10(modbustags.get(beg).address, buffsize, buff) :
					protocol.requestCmd0F(modbustags.get(beg).address, buffsize, buff);
			
			if( !result )
				for(int i=beg; i<end; ++i)
					modbustags.get(i).tag.raiseWriteValue();
				
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
		
		return result;
	}

	
	
	
	private boolean writeSingle(ModbusTagRegion region) {
		int i, size;
		boolean result = false;
		try {
			for(ModbusTag mtag: modbustags) {
				if( mtag.needWrite  &&  mtag.writeSingle  &&  mtag.region == region ) {
					mtag.needWrite = false;
					if( mtag.access == ModbusTagAccess.WO )
						mtag.tag.copyLastWriteToRead();
					
					if( region == ModbusTagRegion.HLDREG ) {
						size = mtag.size;
						adjustBuff(size);
						mtag.putValueIntoBuff(buff, 0);
						for(i=0; i<size; ++i) 
							result = protocol.requestCmd6(mtag.address + i, buff[i]);
					} else
						result = protocol.requestCmd5(mtag.address, mtag.tag.getWriteValInt() );
					
					if(!result) {
						mtag.tag.raiseWriteValue();
						return false;
					}
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
		}
		
		return true;
	}



	private boolean readRegisters(ModbusTagRegion region) {
		
		try {
			boolean isReg = region == ModbusTagRegion.HLDREG  ||  region == ModbusTagRegion.INPREG;
			boolean result = false;
			
			int beg = -1;
			int end = -1;
					
			ModbusTag mtag;
			for(int i=0; i<modbustags.size(); ++i) {
				mtag = modbustags.get(i);
				
				if( mtag.region == region  &&  mtag.access != ModbusTagAccess.WO ) {
					if( beg == -1 )
						beg = i;
					end = i;
				}
				
				if( end >= 0  &&  (modbustags.get(end).readEnd  ||  i == (modbustags.size()-1) )) {
					int addrbeg = modbustags.get(beg).address;
					int size = modbustags.get(end).address + modbustags.get(end).size - addrbeg;
					
					switch(region) {
					case HLDREG:
						result = protocol.requestCmd3(addrbeg, size); break;
					case INPREG:
						result = protocol.requestCmd4(addrbeg, size); break;
					case COIL:
						result = protocol.requestCmd1(addrbeg, size); break;
					case DSCINP:
						result = protocol.requestCmd2(addrbeg, size); break;
					}

					if( !result )
						return false;
					
					for(int j=beg; j<=end; ++j) { 
						mtag = modbustags.get(j);
						if( mtag.region == region  &&  mtag.access != ModbusTagAccess.WO ) {
							if( isReg )
								mtag.fetchValueFromProtocolBuffin(protocol, mtag.address - addrbeg);
							else
								mtag.tag.setReadValBool( (protocol.getAnswerBit(mtag.address - addrbeg) > 0) ^ mtag.inverted );
						}
					}
					
					beg = -1;
					end = -1;
				}
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			return false;
		}
		
		return true;
	}

	
    public void adjustBuff(int size) {
		if( buff.length < size )
			buff = new int[size];
    }

    
	@Override
	protected boolean reload() {

		env.getCmdDispatcher().enableAddCommand(false);
		ModbusModule tmp = new ModbusModule(plugin, name);
		env.getCmdDispatcher().enableAddCommand(true);
		if( !tmp.load() )
			return false;

		copySettingsFrom(tmp);

		maxWriteSizeCoil = tmp.maxWriteSizeCoil;
		maxWriteSizeReg = tmp.maxWriteSizeReg;

		for(ModbusTag mtag: modbustags) {
			Tag found = tmp.tagtable.get(mtag.tag.getName());
			if( found == null  ||  found.getType() != mtag.tag.getType()  ||  !(found instanceof TagRW) ) 
				tagtable.remove(mtag.tag);
		}
		
		for(ModbusTag mtag: tmp.modbustags) {
			TagRW tag = (TagRW)tagtable.get(mtag.tag.getName());
			if( tag == null )
				tagtable.add( mtag.tag );
			else {
				mtag.tag = tag;
			}
		}
		
		modbustags = tmp.modbustags;
		init();
		
		return true;
	}

}
