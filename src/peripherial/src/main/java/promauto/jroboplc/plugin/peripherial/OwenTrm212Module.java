package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.TagRW;

import java.util.List;

public class OwenTrm212Module extends PeripherialModule {
        
        private final Logger logger = LoggerFactory.getLogger(OwenTrm212Module.class);
        
        protected ProtocolModbus protocol = new ProtocolModbus(this);

    protected Tag tagLvoPSTAT; 
    protected Tag tagLvoPPV1;
    protected Tag tagLvoPPV2;
    protected TagRW tagLvoPSP;
          


        
        public OwenTrm212Module(Plugin plugin, String name) {
                super(plugin, name);
        }


        @Override
        public boolean loadPeripherialModule(Object conf) {
                tagLvoPSTAT     = tagtable.createInt("LvoP.STAT", 0, Flags.STATUS);
                tagLvoPPV1      = tagtable.createInt("LvoP.PV1", 0, Flags.STATUS);
                tagLvoPPV2      = tagtable.createInt("LvoP.PV2", 0, Flags.STATUS);
                tagLvoPSP       = protocol.addWriteTag( 0x04, tagtable.createRWInt("LvoP.SP", 0, Flags.STATUS));
                return true;
        }


    @Override
    protected void initChannelMap(List<String> chtags) {
        addChannelMapTag(chtags, tagLvoPPV1, "0");
        addChannelMapTag(chtags, tagLvoPPV2, "1");
        addChannelMapTag(chtags, tagLvoPSP, "sp");
    }

        @Override
        public boolean executePeripherialModule() {             
                if(emulated) {
                    tagLvoPSP.acceptWriteValue();                     
                        return true;
                }
                
                boolean result = true;
                
                try {
                        
                        result = protocol.sendWriteTags(16);                    
                        if( result )
                                if( result = protocol.requestCmd3(0, 3) ) {
                                        tagLvoPSTAT     .setInt(                protocol.getAnswerWord(0) ); 
                                        tagLvoPPV1      .setInt(                protocol.getAnswerWord(1) );
                                        tagLvoPPV2      .setInt(                protocol.getAnswerWord(2) );                                                    
                                }
                                if( result = protocol.requestCmd3(0x04, 1) ) {
                                        tagLvoPSP       .setReadValInt(  protocol.getAnswerWord(0) );
                                }                       
                                            
                        
                } catch (Exception e) {
                        env.printError(logger, e, name);
                        result = false;
                }
        
                return result;
        }

}

