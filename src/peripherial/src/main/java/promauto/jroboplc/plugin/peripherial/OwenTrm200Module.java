package promauto.jroboplc.plugin.peripherial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.api.Flags;
import promauto.jroboplc.core.api.Plugin;
import promauto.jroboplc.core.api.Tag;

import java.util.List;

public class OwenTrm200Module extends PeripherialModule {

	private final Logger logger = LoggerFactory.getLogger(OwenTrm200Module.class);
	
	protected ProtocolModbus protocol = new ProtocolModbus(this);

	protected Tag tagLvoP_STAT;
    protected Tag tagLvoP_PV1;
    protected Tag tagLvoP_PV2;
    protected Tag tagLvoP_LUPV1;
    protected Tag tagLvoP_LUPV2;
    
	
	
	
	public OwenTrm200Module(Plugin plugin, String name) {
		super(plugin, name);
	}


	@Override
	public boolean loadPeripherialModule(Object conf) {

		tagLvoP_STAT 	= tagtable.createInt("LvoP.STAT", 0, Flags.STATUS);
	    tagLvoP_PV1 	= tagtable.createInt("LvoP.PV1", 0, Flags.STATUS);
	    tagLvoP_PV2 	= tagtable.createInt("LvoP.PV2", 0, Flags.STATUS);
	    tagLvoP_LUPV1 	= tagtable.createInt("LvoP.LUPV1", 0, Flags.STATUS);
	    tagLvoP_LUPV2 	= tagtable.createInt("LvoP.LUPV2", 0, Flags.STATUS);

		return true;
	}

	@Override
	protected void initChannelMap(List<String> chtags) {
		addChannelMapTag(chtags, tagLvoP_PV1, "0");
		addChannelMapTag(chtags, tagLvoP_PV2, "1");
	}

	
	@Override
	public boolean executePeripherialModule() {
		
		if(emulated) 
			return true;
		
		boolean result = true;
		
		try {
			if( result = protocol.requestCmd3(0, 5) ) {
				tagLvoP_STAT.setInt(	protocol.getAnswerWord(0) );
				tagLvoP_PV1.setInt(		protocol.getAnswerWord(1) );
				tagLvoP_PV2.setInt(		protocol.getAnswerWord(2) );
				tagLvoP_LUPV1.setInt(	protocol.getAnswerWord(3) );
				tagLvoP_LUPV2.setInt(	protocol.getAnswerWord(4) );
			}
		} catch (Exception e) {
			env.printError(logger, e, name);
			result = false;
		}
	
		return result;
	}
	


}
