package promauto.jroboplc.plugin.peripherial;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

import promauto.jroboplc.core.tags.TagRW;
import promauto.utils.CRC;
import promauto.utils.Numbers;
import promauto.utils.Strings;

public class ProtocolModbus {
	public Charset charset = Charset.forName("UTF-8");

    public static class WriteTag {
		int addr;
		TagRW tag;
		
		public WriteTag(int addr, TagRW tag) {
			this.addr = addr;
			this.tag = tag;
		}
	}

	private PeripherialModule module;
	protected int[] buffin = null;
	protected int[] buffout = null;
	protected List<WriteTag> writeTags = null;
	
	
	public ProtocolModbus(PeripherialModule module) { 
		this.module = module;
	}

	
	public TagRW addWriteTag(int addr, TagRW tag) {
		if( writeTags == null )
			writeTags = new LinkedList<>();
		
		writeTags.add( new WriteTag(addr, tag) );
		return tag;
	}
	
    public boolean requestCmd1(int addr, int size) throws Exception {
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 8;
    	final int sizein = 5 + m;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 1;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd2(int addr, int size) throws Exception {
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 8;
    	final int sizein = 5 + m;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 2;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd3(int addr, int size) throws Exception {
    	final int sizeout = 8;
    	final int sizein = size*2 + 5;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 3;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    
    public boolean requestCmd4(int addr, int size) throws Exception {
    	final int sizeout = 8;
    	final int sizein = size*2 + 5;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 4;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		
		return request(sizeout, sizein, true);
    }
    

    public boolean requestCmd5(int addr, int value) throws Exception {
    	final int sizeout = 8;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 5;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = value > 0? 0xFF: 0;
		buffout[5] = 0;
		
		return request(sizeout, sizein, false);
    }
    
    public boolean requestCmd6(int addr, int value) throws Exception {
    	final int cmd = 6;
    	final int sizeout = 8;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = cmd;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (value >> 8) & 0xFF;
		buffout[5] = value & 0xFF;
		
		return request(sizeout, sizein, false);
    }
    

    public boolean requestCmd0F(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
		int m = size / 8  +  (size % 8 > 0? 1: 0);
    	final int sizeout = 9 + m;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 15;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		buffout[6] = m;
		int mask = 0x100;
		int pos = 6;
		for(int i=0; i<size; ++i) {
			if( mask == 0x100) {
				mask=1;
				pos++;
				buffout[pos] = 0;
			}
			buffout[pos] |= values[i] > 0? mask: 0;
			mask <<= 1;
		}
		
		return request(sizeout, sizein, false);
    }
    

    public boolean requestCmd10(int addr, int size, int... values) throws Exception {
		size = Math.min(values.length, size);
    	final int sizeout = 9 + size * 2;
    	final int sizein = 8;
    	
    	adjustBuffers(sizeout, sizein);
		
		buffout[0] = module.netaddr;
		buffout[1] = 16;
		buffout[2] = (addr >> 8) & 0xFF;
		buffout[3] = addr & 0xFF;
		buffout[4] = (size >> 8) & 0xFF;
		buffout[5] = size & 0xFF;
		buffout[6] = size * 2;
		for(int i=0; i<size; ++i) {
			buffout[7+i*2] = (values[i] >> 8) & 0xFF;
			buffout[8+i*2] = values[i] & 0xFF;
		}
		
		return request(sizeout, sizein, false);
    }
    

    public void adjustBuffers(int sizeout, int sizein) {
		if(buffout == null  ||  buffout.length < sizeout)
			buffout = new int[sizeout];
		
		if(buffin == null  ||  buffin.length < sizein)
			buffin = new int[sizein];
	}


	private boolean request(int sizeout, int sizein, boolean checkBadReq) throws Exception {
		
		int crc = CRC.getCrc16(buffout, sizeout-2);
		buffout[sizeout-2] = crc & 0xFF;
		buffout[sizeout-1] = (crc >> 8) & 0xFF;

		int b = 0;
		for (int trynum = 0; trynum < module.retrial; trynum++) {
			module.port.discard();
			module.delayBeforeWrite();
			module.port.writeBytes(buffout, sizeout);
			
			int cntRead = 0;
			boolean badcrc = false;
			boolean badreq = false;
			for(int i=0; i<sizein; ++i) {
				if ((b = module.port.readByte()) >= 0) {
					buffin[i] = b;
					if(i==1  &&  (b & 0x80)>0 ) {
						sizein = 5;
						badreq = true;
					}
					cntRead++;
				} else
					break;
			}
			
			if(b>=0) {
				int crc1 = CRC.getCrc16(buffin, sizein-2);
				int crc2 = (buffin[sizein-1] << 8) | buffin[sizein-2];

				if( crc1 != crc2 )
					badcrc = true;
				else
					if( buffout[0] == buffin[0]  &&  (!checkBadReq  ||  !badreq) )
						return true;
			}

			if( module.canLogError() )
				module.logError(trynum, badcrc, buffout, sizeout, buffin, cntRead, (badreq?"BadRequest":"") );

			module.delayAfterError();
		}
			
		module.tagErrorCnt.setInt( module.tagErrorCnt.getInt() + 1 );

    	return false;
    }


	
	public boolean sendWriteTags(int cmd) throws Exception {
		if( writeTags == null )
			return true;

		boolean result = true;
		for(WriteTag wt: writeTags) {
			if( wt.tag.hasWriteValue() ) {
				int value = wt.tag.getWriteValInt();
				
				if( cmd == 6 )
					result = requestCmd6(wt.addr, value);
				else if( cmd == 16 )
					result = requestCmd10(wt.addr, 1, value);
				else
					return false;
				
				if( !result )
					break;

				wt.tag.setReadValInt(value);
			}
		}
		
		return result;
	}


	public int getAnswerWord(int num) {
		return Numbers.bytesToWord( buffin, 3 + (num << 1));
	}

	public int getAnswerInt32(int num) {
		return getAnswerInt32(num, false);
	}

	public int getAnswerInt32(int num, boolean littleEndian) {
		if( littleEndian )
			return (getAnswerWord(num+1) << 16) + getAnswerWord(num);
		else
			return (getAnswerWord(num) << 16) + getAnswerWord(num+1);
	}



	public int getAnswerBit(int num) {
		int pos = 3 + num/8;
		int mask = 1 << (num%8);
		return (buffin[pos] & mask) > 0? 1: 0;
	}

	private byte[] buffbytes = new byte[4];
    public float getAnswerFloat(int num) {
        int k = 3 + (num << 1);
        for(int i=0; i<4; ++i)
            buffbytes[i] = (byte)(buffin[k + i]);
        return  ByteBuffer.wrap(buffbytes).getFloat();
    }




    public String getAnswerString(int pos, int size) {
    	int ofs = pos*2 + 3;
		int n = size*2; 
		byte[] bytes = new byte[n];
		for(int i=0; i<n; ++i)
			bytes[i] = (byte)(buffin[ofs + i]);

		return new String(bytes, charset );
	}
	
		

}

