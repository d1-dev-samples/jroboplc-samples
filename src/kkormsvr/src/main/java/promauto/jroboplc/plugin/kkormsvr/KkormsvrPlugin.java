package promauto.jroboplc.plugin.kkormsvr;


import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;

public class KkormsvrPlugin extends AbstractPlugin {
	private static final String PLUGIN_NAME = "kkormsvr";

	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public String getPluginName() {
		return PLUGIN_NAME;
	}

	@Override
	public String getPluginDescription() {
		return "multicomponent doser server";
	}


	@Override
	public Module createModule(String name, Object conf) {
    	Module m = createDatabaseModule(name, conf);
    	if( m != null )
    		modules.add(m);
    	return m;
		
	}
	
	private Module createDatabaseModule(String name, Object conf) {
		KkormsvrModule m = new KkormsvrModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}



}
