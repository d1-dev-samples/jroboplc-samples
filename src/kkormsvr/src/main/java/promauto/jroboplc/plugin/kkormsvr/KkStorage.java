package promauto.jroboplc.plugin.kkormsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Tag;
import promauto.jroboplc.core.tags.RefGroup;
import promauto.jroboplc.core.tags.RefItem;
import promauto.jroboplc.core.tags.TagRW;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class KkStorage {
    private final Logger logger = LoggerFactory.getLogger(KkStorage.class);

    public final KkormsvrModule module;
    private final KkDoser doser;

    private int id;
    private int addr;
    private int stornum;
    private String name;


    // --- live data ---
    private RefItem refSumWeight;
            RefItem refReqWeight;
    private RefItem refSetWeight;
    // --- live data ---

    private long sumWeight = -1;
            long reqWeight;
    private int  productId;

    private String prmSumWeight;
    private String prmReqWeight;
    private String prmProductId;

    private Tag   tagReqWeight;
    private TagRW tagProductName;


    KkStorage(KkDoser doser) {
        this.doser = doser;
        this.module = doser.module;
    }

    boolean load(int addr, Object numname) {
        try {
            this.addr = addr;
            if (numname instanceof Integer) {
                stornum = (Integer) numname;
                name = stornum + " Бункер";
            } else {
                Pattern p = Pattern.compile("(\\d+)\\s*,\\s*(\\S+.*)");
                Matcher m = p.matcher(numname.toString());
                if (!m.find())

                    throw new IllegalArgumentException();
                stornum = Integer.parseInt( m.group(1) );
                name = m.group(2);
            }
        } catch (Exception e) {
            EnvironmentInst.get().printError(logger, module.getName(), doser.getName(),
                    "Bad storage num / name: " + addr + ":" + numname);
            return false;
        }


        TagTable tt    = module.getTagTable();
        tagProductName = tt.createRWString( doser.prefix() + "Product"   + addr, "");
        tagReqWeight   = tt.createLong(     doser.prefix() + "ReqWeight" + addr, 0);

        return true;
    }

    void createRefs(RefGroup refgr, String bindModuleName) {
        refSumWeight = refgr.createItemCrc(bindModuleName, "SumWeight" + addr);
        refReqWeight = refgr.createItem(   bindModuleName, "ReqWeight" + addr);
        refSetWeight = refgr.createItem(   bindModuleName, "SetWeight" + addr);
    }

//    String prefix() {
//        return doser.prefix() + "stor" + addr + ".";
//    }

    // --- live data ---
    void initLiveData() {
        prmSumWeight = doser.prefix() + "SumWeight" + addr;
        prmReqWeight = doser.prefix() + "ReqWeight" + addr;
        prmProductId = doser.prefix() + "ProcudtId" + addr;
    }

    void readLiveData() {
        sumWeight = module.livedata.getLong(prmSumWeight, sumWeight);
        reqWeight = module.livedata.getLong(prmReqWeight, reqWeight);
        productId = module.livedata.getInt(prmProductId, productId);
    }

    void updateLiveData() {
        module.livedata.set(prmSumWeight, sumWeight);
        module.livedata.set(prmReqWeight, reqWeight);
        module.livedata.set(prmProductId, productId);
    }




    void init() throws SQLException {
        Statement st = module.getStatement();

        // kk_storage
        String sql = String.format(
                "select id, name from kk_storage where stornum=%d",
                stornum);

        try(ResultSet rs = st.executeQuery(sql)) {
            if (rs.next()) {
                id = rs.getInt("id");
                if( !rs.getString("name").equals(name)) {
                    sql = String.format(
                            "update kk_storage set name='%s' where id=%d",
                            name,
                            id);
                    st.executeUpdate(sql);
                }
            } else {
                // insert new
                sql = String.format(
                        "insert into kk_storage (stornum, name) values (%d, '%s')",
                        stornum,
                        name);
                id = module.getDatabase().insertReturningId(st, sql);
            }
        }

        // kk_linestor
        sql = String.format(
                "select 1 from kk_linestor where line_id=%d and storage_id=%d",
                doser.line.getId(),
                id);

        try(ResultSet rs = st.executeQuery(sql)) {
            if (!rs.next()) {
                sql = String.format(
                        "insert into kk_linestor (line_id, storage_id, kg) values (%d, %d, 0)",
                        doser.line.getId(),
                        id);
                st.executeUpdate(sql);
            }
        }

        sql = String.format(
                "delete from kk_linestor where line_id<>%d and storage_id=%d",
                doser.line.getId(),
                id);
        st.executeUpdate(sql);


        // fetch product name
        if( productId > 0 ) {
            sql = String.format(
                    "select name from kk_product where id=%d",
                    productId);

            try(ResultSet rs = st.executeQuery(sql)) {
                if (rs.next()) {
                    tagProductName.setReadValString(rs.getString(1));
                }
            }
        }

    }


    // main loop
    void execute() throws SQLException {
        refSetWeight.getTag().setLong(reqWeight);

        if( sumWeight == -1 )
            sumWeight = refSumWeight.getValue().getLong();

        long weight = refSumWeight.getValue().getLong() - sumWeight;
        if( weight != 0 ) {
            if( weight < 0 )
                weight += doser.getSumWeightMax() + 1;

            doser.line.saveWeight(
                    id,
                    productId,
                    doser.getGroup().getCycleCnt(),
                    weight,
                    sumWeight,
                    refSumWeight.getValue().getLong()
            );


            sumWeight = refSumWeight.getValue().getLong();
        }

        updateTags();
    }

    private void updateTags() {
        tagReqWeight.setLong(reqWeight);
    }



    void applyTask(KkTask task) {
        KkTask.Detail detail = task.details.get(id);
        if( detail != null) {
            reqWeight = detail.weight;
            productId = detail.productId;
            tagProductName.setReadValString( detail.productName );
            detail.applied = true;
        } else {
            reqWeight = 0;
            productId = 0;
            tagProductName.setReadValString( "" );
        }
    }


    public String getInfo() {
        return String.format("      %2d: sum=%-10d set=%-10d %d/%s [%s]",
                addr, sumWeight, reqWeight, stornum, name, tagProductName);
    }

    // --- getters ---
    public int getAddr() {
        return addr;
    }

    public int getNum() {
        return stornum;
    }

    public String getName() {
        return name;
    }

    public String getProduct() {
        return tagProductName.getString();
    }

}

