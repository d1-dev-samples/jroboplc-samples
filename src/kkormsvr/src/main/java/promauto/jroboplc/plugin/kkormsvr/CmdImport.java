package promauto.jroboplc.plugin.kkormsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DbHelper;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.EnvironmentInst;
import promauto.jroboplc.core.api.Module;

public class CmdImport extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdImport.class);


	@Override
	public String getName() {
		return "import";
	}

	@Override
	public String getUsage() {
		return "db";
	}

	@Override
	public String getDescription() {
		return "imports kkormsvr data from database db";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {
		
		KkormsvrModule w = (KkormsvrModule) module;

		Database dbdst = w.getDatabase();
		if( !dbdst.isConnected() ) {
			console.print( "Database " + dbdst.getName() + " is not connected\n" );
			return;
		}

		Module m = EnvironmentInst.get().getModuleManager().getModule(args);
		if( !(m instanceof Database) ) {
			console.print( "Module " + args + " is not a database\n" );
			return;
		}
		Database dbsrc = (Database)m;
		if( !dbsrc.isConnected() ) {
			console.print( "Database " + dbsrc.getName() + " is not connected\n" );
			return;
		}

		DbHelper.clear(console, dbdst,
				"kk_editlog, kk_rashod, kk_output, kk_execute, kk_taskcontent, kk_task, " +
						"kk_linestor, kk_line, kk_storage, kk_rcpprod, kk_receipt, kk_product, " +
						"kk_userpriv, users, kk_livedataval, kk_livedata");


		DbHelper.copytbl(console, dbsrc, dbdst, "users", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_userpriv", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_product", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_receipt", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_rcpprod", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_storage", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_line", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_linestor", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_task", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_taskcontent", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_execute", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_output", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_rashod", "*");
		DbHelper.copytbl(console, dbsrc, dbdst, "kk_editlog", "*");


		DbHelper.copygen(console, dbdst, "kk_execute",		"gen_kk_execute_id", 	"id");
		DbHelper.copygen(console, dbdst, "kk_line", 			"gen_kk_line_id", 		"id");
		DbHelper.copygen(console, dbdst, "kk_output", 		"gen_kk_output_id",     "id");
		DbHelper.copygen(console, dbdst, "kk_privilegs",		"gen_kk_privilegs_id", 	"idpriv");
		DbHelper.copygen(console, dbdst, "kk_product", 		"gen_kk_product_id", 	"id");
		DbHelper.copygen(console, dbdst, "kk_rashod", 		"gen_kk_rashod_id", 	"id");
		DbHelper.copygen(console, dbdst, "kk_rcpprod", 		"gen_kk_rcpprod_id", 	"id");
		DbHelper.copygen(console, dbdst, "kk_receipt", 		"gen_kk_receipt_id", 	"id");
		DbHelper.copygen(console, dbdst, "kk_storage", 		"gen_kk_storage_id",	"id");
		DbHelper.copygen(console, dbdst, "kk_taskcontent",	"gen_kk_taskcontent_id","id");
		DbHelper.copygen(console, dbdst, "kk_task", 			"gen_kk_task_id", 		"id");
		DbHelper.copygen(console, dbdst, "users", 			"gen_users_id", 		"iduser");


		console.print("Done\n");
	}


}
