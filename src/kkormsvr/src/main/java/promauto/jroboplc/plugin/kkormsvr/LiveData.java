package promauto.jroboplc.plugin.kkormsvr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

class LiveData {
    private final KkormsvrModule module;
    private boolean hasChanges;

    private static class Rec {
        int id;
        String name;
        long value;
        boolean hasChanges;

        Rec(int id, String name, long value, boolean hasChanges) {
            this.id = id;
            this.name = name;
            this.value = value;
            this.hasChanges = hasChanges;
        }
    }

    private Map<String, Rec> data = new HashMap<>();

    LiveData(KkormsvrModule module) {
        this.module = module;
        hasChanges = false;
    }


    // getters and setters
    int getInt(String name, int defval) {
        Rec rec = data.get(name);
        if( rec == null )
            return defval;
        return (int)rec.value;
    }

    long getLong(String name, long defval) {
        Rec rec = data.get(name);
        if( rec == null )
            return defval;
        return rec.value;
    }

    void set(String name, long value) {
        Rec rec = data.get(name);
        if( rec == null ) {
            rec = new Rec(0, name, value, true);
            data.put(name, rec);
            hasChanges = true;
        } else {
            if (rec.value != value) {
                rec.value = value;
                rec.hasChanges = true;
                hasChanges = true;
            }
        }
    }


    void load() throws SQLException {
        data.clear();
        hasChanges = false;

        String sql =
                "select ld.id, ld.name, ldv.val from kk_livedata ld " +
                "left join kk_livedataval ldv on ldv.id = ld.id";

        try(ResultSet rs = module.getStatement().executeQuery(sql)) {
            while (rs.next()) {
                data.put(
                        rs.getString("name"),
                        new Rec(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getLong("val"),
                                false));
            }
        }
    }



    void update() throws SQLException {
        if( !hasChanges )
            return;

        Statement st = module.getStatement();
        for(Rec rec: data.values())
            if( rec.hasChanges ) {
                String sql;
                if( rec.id == 0 ) {
                    sql = String.format(
                            "insert into kk_livedata (name) values ('%s')",
                            rec.name);
                    rec.id = module.getDatabase().insertReturningId(st, sql);
                } else {
                    sql = String.format(
                            "delete from kk_livedataval where id=%d",
                            rec.id);
                    st.executeUpdate(sql);
                }

                sql = String.format(
                        "insert into kk_livedataval (id, val) values (%d, %d)",
                        rec.id,
                        rec.value);
                st.executeUpdate(sql);

                rec.hasChanges = false;
            }

        hasChanges = false;
    }


}
