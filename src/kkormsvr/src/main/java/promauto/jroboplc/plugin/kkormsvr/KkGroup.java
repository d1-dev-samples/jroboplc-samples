package promauto.jroboplc.plugin.kkormsvr;

import promauto.jroboplc.core.TagTable;
import promauto.jroboplc.core.api.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class KkGroup {
    private final static int STATE_IDLE     = 0;
    private final static int STATE_START    = 1;
    private final static int STATE_TASK     = 2;
    private final static int STATE_DOSING   = 3;
    private final static int STATE_ERROR    = 4;

    public final KkormsvrModule module;
    public final KkLine line;
    public final int level;

    // --- live data ---
    private int cycleCnt;
    private int state;
    // --- live data ---
    private String prmCycleCnt;
    private String prmState;

    private Tag tagCycleCnt;
    private Tag tagState;


    private List<KkDoser> dosers = new ArrayList<>();


    KkGroup(KkLine line, int level) {
        this.line = line;
        this.module = line.module;
        this.level = level;
    }

    void addDoser(KkDoser doser) {
        dosers.add(doser);
    }

    void load() {
        TagTable tt = module.getTagTable();
        tagCycleCnt = tt.createInt(prefix() + "CycleCnt", 0);
        tagState = tt.createInt(prefix() + "State", 0);
    }

    private String prefix() {
        return line.prefix() + "Group" + level + ".";
    }

    // --- live data ---
    void initLiveData() {
        prmCycleCnt = prefix() + "CycleCnt";
        prmState = prefix() + "State";
    }

    void readLiveData() {
        cycleCnt = module.livedata.getInt(prmCycleCnt, cycleCnt);
        state = module.livedata.getInt(prmState, state);
    }

    void updateLiveData() {
        module.livedata.set(prmCycleCnt, cycleCnt);
        module.livedata.set(prmState, state);
    }


    // main loop
    void execute() {

        if( state == STATE_IDLE) {
            if( line.isRunning()  &&  canStartCycle() )
                state = STATE_START;
        }

        if( state == STATE_START) {
            prepareTaskSend();
            if( isTaskSendReady() ) {
                sendTask();
                state = STATE_TASK;
            }
        }

        if( state == STATE_TASK) {
            if( isTaskAccepted() ) {
                startDosing();
                state = STATE_DOSING;
            } else

            if( isTaskRejected() )
                state = STATE_ERROR;
        }

        if( state == STATE_DOSING) {
            if( isDosingFinished() ) {
                state = STATE_IDLE;
                cycleCnt++;
            }
        }

//        if( state == STATE_ERROR ) {
//            // waiting for group reset
//        }

        updateTags();
    }



    private void updateTags() {
        tagCycleCnt.setInt(cycleCnt);
        tagState.setInt(state);
    }



    void reset() {
        state = STATE_IDLE;
        cycleCnt = 0;
    }


    private boolean canStartCycle() {
        return cycleCnt < line.getCycleReq()
                &&  cycleCnt <= line.getCycleCnt() + level;
    }

    private void prepareTaskSend() {
        dosers.stream()
                .filter(KkDoser::isHasTask)
                .forEach(KkDoser::prepareTaskSend);
    }

    private boolean isTaskSendReady() {
        return dosers.stream()
                .filter(KkDoser::isHasTask)
                .allMatch(KkDoser::isTaskSendReady);
    }

    private void sendTask() {
        dosers.stream()
                .filter(KkDoser::isHasTask)
                .forEach(KkDoser::sendTask);
    }

    private boolean isTaskAccepted() {
        return dosers.stream()
                .filter(KkDoser::isHasTask)
                .allMatch(KkDoser::isTaskAccepted);
    }

    private boolean isTaskRejected() {
        return dosers.stream()
                .filter(KkDoser::isHasTask)
                .allMatch(KkDoser::isTaskRejected);
    }

    private void startDosing() {
        dosers.stream()
                .filter(KkDoser::isHasTask)
                .forEach(KkDoser::startDosing);
    }

    private boolean isDosingFinished() {
        return dosers.stream()
                .filter(KkDoser::isHasTask)
                .allMatch(KkDoser::isDosingFinished);
    }


    int getCycleCnt() {
        return cycleCnt;
    }


    boolean isIdle() {
        return state == STATE_IDLE;
    }

    boolean isError() {
        return state == STATE_ERROR;
    }


    public String getInfo() {
        return "  group" + level + ": " + getStateStr() + " cnt=" + cycleCnt + "\r\n"
                + dosers.stream()
                    .map(KkDoser::getInfo)
                    .collect(Collectors.joining("\r\n"));
    }

    private String getStateStr() {
        switch (state) {
            case STATE_IDLE:     return "IDLE";
            case STATE_START:    return "START";
            case STATE_TASK:     return "TASK";
            case STATE_DOSING:   return "DOSING";
            case STATE_ERROR:    return "ERROR";
        }
        return "";
    }


}
