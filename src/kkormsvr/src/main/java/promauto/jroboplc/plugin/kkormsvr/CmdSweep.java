package promauto.jroboplc.plugin.kkormsvr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import promauto.jroboplc.core.AbstractCommand;
import promauto.jroboplc.core.DbHelper;
import promauto.jroboplc.core.api.Console;
import promauto.jroboplc.core.api.Database;
import promauto.jroboplc.core.api.Module;
import promauto.jroboplc.plugin.wessvr.ArchiveConfig;
import promauto.jroboplc.plugin.wessvr.WessvrModule;

public class CmdSweep extends AbstractCommand {
	private final Logger logger = LoggerFactory.getLogger(CmdSweep.class);


	@Override
	public String getName() {
		return "sweep";
	}

	@Override
	public String getUsage() {
		return "year";
	}

	@Override
	public String getDescription() {
		return "deletes old records from archive tables for specified year and all before (dtbeg <= 'year.12.31 23:59:59.999')";
	}

	
	
	
	@Override
	public String execute(Console console, Module module, String args) {
		module.postCommand(this, console, module, args);
		return "";
	}


	@Override
	public void executePosted(Console console, Module module, String args) {

        KkormsvrModule w = (KkormsvrModule) module;

        Database db = w.getDatabase();
        if (!db.isConnected()) {
            console.print("Database " + db.getName() + " is not connected\n");
            return;
        }

        int year;
        try {
            year = Integer.parseInt(args);
        } catch (NumberFormatException e) {
            console.print("Invalid year: " + args + "\n");
            return;
        }

        console.print("\n");
		DbHelper.sweep(console, db, "kk_rashod","dt", year);
		DbHelper.sweep(console, db, "kk_output","dt", year);
		DbHelper.sweep(console, db, "kk_execute","dt", year);
		DbHelper.sweep(console, db, "kk_task","dt", year);
        console.print("OK\n");
    }



}
