package promauto.jroboplc.plugin.jrbustcp;

import promauto.utils.Strings;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.zip.CRC32;


/*
Before run start server:

--- begin of file: "jrbustcp-testserver.yml" ---

plugin.jrbustcp:
  module.jrsvr:
    type:        server
    port:        30000

plugin.script:
  module.scr:
    var:
      - inp0;type:bool=off
      - inp1;type:bool=on
      - Control
      - State
      - Text;type:string=Hello, world!
      - Value;type:double=12.34

plugin.task:
  module.task_main:
    period:   1000
    modules:
      - scr

--- end of file ---
 */

public class ServerTester {


    private static final int TIMEOUT = 500;
    private static OutputStream output;
//    private static String hostname = "localhost";
//    private static int port = 30000;
    private static String hostname = "192.168.1.44";
    private static int port = 35000;
    private static Socket socket;
    private static CRC32 crc = new CRC32();


    private static String str(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%02X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(String s) {
        byte[] bb = s.getBytes(Charset.defaultCharset());
        String ss = String.format("%04X ", bb.length) + Strings.bytesToHexString(bb);
        return ss;
    }

    private static String val(Double val) {
        return String.format("%08X ", Double.doubleToLongBits(val));
    }


    private static String addHeaderFooter(String s) {
        s = "FF8888FF" + s.replace(" ","");
        byte[] bb = Strings.hexStringToBytes(s);
        crc.reset();
        crc.update(bb);
        s = "ABCD" + s + String.format("%08X", crc.getValue());
        int len = s.length()/2;
        s = String.format("%04X", len) + s;
        return s;
    }

    private static void send(String s) throws IOException {
        String ss = addHeaderFooter(s);
        byte[] bb = Strings.hexStringToBytes(ss);
        System.out.println( "\r\nout: " + Strings.bytesToHexString(bb) );
        output.write(bb);
    }

    private static void receive(String s) throws Exception {
        InputStream input = socket.getInputStream();
        byte[] bb = new byte[1000];
        int k = 0;
        int b;
        try {
            while ((b = input.read()) != -1 && k < 1000) {
                bb[k++] = (byte) b;
            }
        } catch (Exception e) {
        }

        String act = Strings.bytesToHexString(Arrays.copyOfRange(bb, 0, k));
        String exp = Strings.bytesToHexString( Strings.hexStringToBytes(addHeaderFooter(s)) );

        System.out.println("in:  " + act );

        if( s.isEmpty() ) {
            if( bb.length > 0)
                return;
            throw new Exception("No anwer");
        }

        if( !act.equals(exp) ) {
            System.out.println("exp: " + exp );
            throw new Exception("Expected and actual differ");
        }

    }


    public static void main(String[] args) {

        try {
            socket = new Socket(hostname, port);
            socket.setSoTimeout(TIMEOUT);
            output = socket.getOutputStream();

            test();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//=============================================== Tests ===============================================

    // check list, write all, reinit
    private static void test1() throws Exception {
        // init
        send("01" + str("scr.*") + str("testclient1"));
        receive("81 00 06");

        // list
        send("02 00 00");
        receive(String.format("82 0000 0006 0000 02%s00 02%s00 05%s00 04%s00 01%s00 01%s00"
                , str("scr.Control")
                , str("scr.State")
                , str("scr.Text")
                , str("scr.Value")
                , str("scr.inp0")
                , str("scr.inp1")
        ));

        // update
        send("03");
        receive("83 0006 00");

        // read all
        send("04 0000");
        receive("");

        // write all
        send(String.format("05 0000 0006 F0 F0 FB%s FA%s F0 F0"
                , val("hello")
                , val(12.34)
        ));
        receive("85 00");


        // init select two tags
        send("01" + str("scr.inp.*") + str("testclient1"));
        receive("81 00 02");

        // list
        send("02 00 00");
        receive(String.format("82 0000 0002 0000 01%s00 01%s00"
                , str("scr.inp0")
                , str("scr.inp1")
        ));


        // init select zero tags
        send("01" + str("badfilter") + str("testclient1"));
        receive("81 00 00");

        // list
        send("02 00 00");
        receive("82 0000 0000 0000");



    }


    // check list, read all
    private static void test2() throws Exception {
        // init
        send("01" + str("scr.*") + str("testclient2"));
        receive("81 00 06");

        // list
        send("02 00 00");
        receive(String.format("82 0000 0006 0000 02%s00 02%s00 05%s00 04%s00 01%s00 01%s00"
                , str("scr.Control")
                , str("scr.State")
                , str("scr.Text")
                , str("scr.Value")
                , str("scr.inp0")
                , str("scr.inp1")
        ));

        // update
        send("03");
        receive("83 0006 00");

        // read all
        send("04 0000");
        receive(String.format("84 0000 0006 0000 F0 F0 FB%s FA%s F0 F0"
                , val("hello")
                , val(12.34)
        ));

        // read all again
        send("04 0000");
        receive(String.format("84 0000 0006 0000 F0 F0 FB%s FA%s F0 F0"
                , val("hello")
                , val(12.34)
        ));
    }


    // random read
    private static void test3() throws Exception {
        // init
        send("01" + str("scr.*") + str("testclient3"));
        receive("");

        // update
        send("03");
        receive("83 0006 00");

        // read 4,5
        send("04 0004");
        receive("84 0004 0002 0000  F0 F0");

        // update
        send("03");
        receive("83 0004 00");

        // read 1,2,3
        send("04 0001");
        receive(String.format("84 0001 0003 0000 F0 FB%s FA%s"
                , val("hello")
                , val(12.34)
        ));

        // update
        send("03");
        receive("83 0001 00");

        // read 0
        send("04 0000");
        receive("84 0000 0001 0000 F0");
    }

    // random write
    private static void test4() throws Exception {
        // init
        send("01" + str("scr.*") + str("testclient4"));
        receive("");

        // update
        send("03");
        receive("83 0006 00");

        // read all
        send("04 0000");
        receive("");



        // write 1, 5
        send("05 0001 0002 F2AA FF0005 F1");
        receive("85 00");

        // update
        send("03");
        receive("83 0002 00");

        // read 1, 5
        send("04 0000");
        receive("84 0001 0002 0000 F2AA FF0005 F1");


        // write 3, 4
        send(String.format("05 0003 0002 FA%s F1"
                , val(2.345)
        ));
        receive("85 00");

        // update
        send("03");
        receive("83 0002 00");

        // read 3, 4
        send("04 0001");
        receive(String.format("84 0003 0002 0000 FA%s F1"
                , val(2.345)
        ));


        // write 2
        String longstring = " 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789 123456789";
        send(String.format("05 0002 0001 FB%s"
                , val(longstring)
        ));
        receive("85 00");

        // write 0
        send("05 0000 0001 F811223344");
        receive("85 00");


        // update
        send("03");
        receive("83 0002 00");

        // read 3, 4
        send("04 0000");
        receive(String.format("84 0000 0002 0000 F811223344 FF0002 FB%s"
                , val(longstring)
        ));



        // write 0, 1
        send("05 0000 0002 F34477 F35588");
        receive("85 00");

        // update
        send("03");
        receive("83 0002 00");

        // read 3, 4
        send("04 0000");
        receive("84 0000 0002 0000 F34477 F35588");



        // error write 6
        send("05 0000 0006 F811223344");
        receive("85 FF");


    }

    private static void test5() throws Exception {
        // init
        send("01" + str("scr.*") + str("testclient4"));
        receive("81 00 06");

        // write 2 = 0
        send("05 0002 0001 F0");
        receive("85 00");

        // update
        send("03");
        receive("83 0006 00");

        // read all
        send("04 0000");
        receive("");



        // write абвгедейка
        String russtring = "абвгедейка";
//        String russtring = "abcd";
        send(String.format("05 0002 0001 FB%s"
                , val(russtring)
        ));
        receive("85 00");

        // update
        send("03");
        receive("83 0001 00");

        // read 3, 4
        send("04 0000");
        receive(String.format("84 0002 0001 0000 FB%s"
                , val(russtring)
        ));


    }

    private static void test6() throws Exception {
        // init
        send("01" + str(".*") + str("testclient6"));
        receive("");

        // update
        send("03");
        receive("");

        for(int i=0; i<10; ++i) {
            System.out.println("---- " + i + " ----");

            send("03");
            receive("");

            // read all
            send("04 0000");
            receive("");
        }

    }

    private static void test7() throws Exception {
//      String ss = "00 1a ab cd fe 4d 0f 57 01 02 2e 2a 09 62 6f 74 74 69 6c 69 6e 69 00 01 f3 9d 67 cc";
        String ss = "00 1a ab cd fe 4d 0f 50 01 02 2e 2a 09 62 6f 74 74 69 6c 69 6e 69 00 01 d3 0f 1b 87";
        byte[] bb = Strings.hexStringToBytes(ss);
        System.out.println("\r\nout: " + Strings.bytesToHexString(bb));
        output.write(bb);

        receive("");
    }


    private static void test() throws Exception {
//        test1();
//        test2();
//        test3();
//        test4();
//        test5();
        test7();
        System.out.println("\r\nSuccess!\r\n");
    }

}
