package promauto.jroboplc.plugin.jrbustcp;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.compression.ZlibCodecFactory;
import io.netty.handler.codec.compression.ZlibWrapper;
import io.netty.handler.ssl.SslContext;

import static promauto.jroboplc.plugin.jrbustcp.JrbustcpProtocol.FRAME_SIZE_MAX;

public class ClientInitializer extends ChannelInitializer<SocketChannel> {
    private static final int TRAFFIC_CHECK_INTERVAL_S = 3;
    private final JrbustcpClientModule module;
    private final SslContext sslCtx;


    public ClientInitializer(JrbustcpClientModule module, SslContext sslCtx) {
        this.module = module;
        this.sslCtx = sslCtx;
    }

    @Override
    public void initChannel(SocketChannel ch) {
        ChannelPipeline pipeline = ch.pipeline();

//        if( module.isTraffic() ) {
//            pipeline.addFirst(new ChannelTrafficShapingHandler(0, 0, TRAFFIC_CHECK_INTERVAL_S * 1000) {
//                @Override
//                protected void doAccounting(TrafficCounter counter) {
//                    module.setMetricsValues(
//                            counter.lastReadBytes()/TRAFFIC_CHECK_INTERVAL_S,
//                            counter.lastWrittenBytes()/TRAFFIC_CHECK_INTERVAL_S);
//                }
//            });
//        }

        pipeline.addFirst("traffic", new TrafficHandler());

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc()));
        }

        if (module.getCompress().equals("gzip")) {
            pipeline.addLast(ZlibCodecFactory.newZlibEncoder(ZlibWrapper.GZIP));
            pipeline.addLast(ZlibCodecFactory.newZlibDecoder(ZlibWrapper.GZIP));
        }

        pipeline.addLast(new LengthFieldBasedFrameDecoder(FRAME_SIZE_MAX, 0, 2, 0, 2));
        pipeline.addLast(new LengthFieldPrepender(2));

        pipeline.addLast(new ClientHandler(module));
    }

}
