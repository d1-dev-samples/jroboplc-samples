package promauto.jroboplc.plugin.jrbustcp;

import io.netty.handler.traffic.ChannelTrafficShapingHandler;
import io.netty.handler.traffic.TrafficCounter;

public class TrafficHandler extends ChannelTrafficShapingHandler {
    private static final int TRAFFIC_CHECK_INTERVAL_S = 3;
    private long counterRead;
    private long counterWritten;

    public TrafficHandler() {
        super(0, 0, TRAFFIC_CHECK_INTERVAL_S * 1000);
    }

    public long getCounterRead() {
        return counterRead;
    }

    public long getCounterWritten() {
        return counterWritten;
    }

    @Override
    protected void doAccounting(TrafficCounter counter) {
          counterRead = counter.lastReadBytes()/TRAFFIC_CHECK_INTERVAL_S;
          counterWritten = counter.lastWrittenBytes()/TRAFFIC_CHECK_INTERVAL_S;
    }
}
