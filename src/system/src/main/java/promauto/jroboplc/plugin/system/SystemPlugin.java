package promauto.jroboplc.plugin.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import promauto.jroboplc.core.AbstractPlugin;
import promauto.jroboplc.core.api.Module;
import promauto.utils.CRC;

public class SystemPlugin extends AbstractPlugin {
	private final Logger logger = LoggerFactory.getLogger(SystemPlugin.class);

	private static final String PLUGIN_NAME = "system";
	private static final String LIB_VERSION = "1.0";

	public enum OsType {
		Linux,
		Windows
	}
	private OsType osType;
	private String osName;
	private String architecture;

	private String resourceName = "";
	private boolean libLoaded = false;
	private boolean firstCheckLibLoaded = true;

	
	@Override
	public void initialize() {
		super.initialize();
		
		initOsType();
		
		initArchitecture();
        
        initLibrary();
        
    }
	
	@Override
    public String getPluginName() {
    	return PLUGIN_NAME;
    }

	@Override
    public String getPluginDescription(){
    	return "Platform specific features";
    }
	

	@Override
	public Module createModule(String name, Object conf) {
		SystemModule m = new SystemModule(this, name);
    	modules.add(m);
    	return m.load(conf)? m: null;
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void initOsType() {
		osName = System.getProperty("os.name");
        if(osName.equals("Linux")){
        	osType = OsType.Linux;
            osName = "linux";
        }
        else if(osName.startsWith("Win")){
        	osType = OsType.Windows;
            osName = "windows";
        }
	}
	
	public OsType getOsType() {
		return osType;
	}
	
	public String getOsName() {
		return osName;
	}

	
	private void initArchitecture() {
        architecture = System.getProperty("os.arch");
        String javaLibPath = System.getProperty("java.library.path");
        if(architecture.equals("i386") || architecture.equals("i686")) {
            architecture = "x86";
        }
        else if(architecture.equals("amd64") || architecture.equals("universal")) {
            architecture = "x86_64";
        }
        else if(architecture.equals("arm")) {
            String floatStr = "sf";
            if(javaLibPath.toLowerCase().contains("gnueabihf") || javaLibPath.toLowerCase().contains("armhf")){
                floatStr = "hf";
            } else {
                try {
                    Process readelfProcess =  Runtime.getRuntime().exec("readelf -A /proc/self/exe");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(readelfProcess.getInputStream()));
                    String buffer;
                    while((buffer = reader.readLine()) != null && !buffer.isEmpty()){
                        if( buffer.toLowerCase().contains("Tag_ABI_VFP_args".toLowerCase()) ) {
                            floatStr = "hf";
                            break;
                        }
                    }
                    reader.close();
                }
                catch (Exception ex) {}
            }
            architecture = "arm" + floatStr;
        }
	}

	
	private void initLibrary() {
        String libname = PLUGIN_NAME + '-' + LIB_VERSION + '_' + architecture;
        libname = System.mapLibraryName(libname);

        Path libfile = Paths.get(env.getModuleManager().getPluginDir()).resolve("..").resolve("libjni").resolve(libname).toAbsolutePath();

        resourceName = "libjni/" + osName + "/" + libname;
        
        if( !libLoaded ) 
        	if( extractResource(libfile, resourceName) )
        		try {     
        			System.load(libfile.toString());
        			libLoaded = true;
        		} catch( Exception e ) {
    				env.printError(logger, e, PLUGIN_NAME, "initialize");
        		}
        
	}


	public boolean checkLibLoaded() {
		if( firstCheckLibLoaded ) {
			firstCheckLibLoaded = false;
			if (!libLoaded)
				env.printError(logger, PLUGIN_NAME, "Resource not found:", resourceName);
		}

		return libLoaded;
	}

	
	private boolean extractResource(Path libFilePath, String resourceName) {
        boolean res = false;
        
        URLClassLoader cl = env.getModuleManager().getClassLoader();
        URL resourceUrl = cl.getResource(resourceName);
        if( resourceUrl == null ) {
        	return false;
        }

        boolean libFilePathExists = Files.exists(libFilePath);
		if (libFilePathExists) {
			try {
				StringBuilder sb1 = new StringBuilder();
				StringBuilder sb2 = new StringBuilder();

				URLConnection con = resourceUrl.openConnection();
				if( CRC.getMD5(con.getInputStream(), sb1)  &&  CRC.getMD5(libFilePath, sb2) )
					if( sb1.toString().equals( sb2.toString() ))
						return true;

			} catch (IOException e) {
				env.printError(logger, e, PLUGIN_NAME, "Extract resource:", resourceName);
				return false;
			}
		}

        InputStream input = cl.getResourceAsStream(resourceName);
        OutputStream output = null;
        
        if(input != null) {
    		env.printInfo(logger, (libFilePathExists? "Updating": "Extracting") +  " resource:", 
    				libFilePath.getFileName().toString());

			int read;
            byte[] buffer = new byte[4096];
            try {
    			Files.createDirectories(libFilePath.getParent());
                output = Files.newOutputStream(libFilePath);
                while((read = input.read(buffer)) != -1)
                    output.write(buffer, 0, read);
                output.close();
                input.close();
                res = true;
            } catch (Exception ex) {
                try {
                    output.close();
                    if(Files.exists(libFilePath))
        				Files.delete(libFilePath);
                } catch (Exception e1) {}
                
                try {
                    input.close();
                } catch (Exception e2) {}
            }
        }
        return res;
    }
	
	
	public boolean setHardwarePortPermission(int address) {
		return checkLibLoaded()?
				setHardwarePortPermissionNative(address) == 0
				: false;
	}
	
	public int readHardwarePort(int address) {
		return checkLibLoaded()?
				readHardwarePortNative(address)
				: 0;
	}
	
	public void writeHardwarePort(int address, int value) {
		if(checkLibLoaded())
			writeHardwarePortNative(address, value);
	}
	

	
	private native int setHardwarePortPermissionNative(int address);
	private native int readHardwarePortNative(int address);
	private native void writeHardwarePortNative(int address, int value);

}
