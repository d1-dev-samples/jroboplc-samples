#include <jni.h>
#include "../promauto_jroboplc_plugin_system_SystemPlugin.h"

#include <sys/io.h>
#include <errno.h>




JNIEXPORT jint JNICALL Java_promauto_jroboplc_plugin_system_SystemPlugin_setHardwarePortPermissionNative
  (JNIEnv *, jobject, jint _address)
{
    if( ioperm(_address, 1, 1) )
        return errno;

    return 0;
}




JNIEXPORT jint JNICALL Java_promauto_jroboplc_plugin_system_SystemPlugin_readHardwarePortNative
  (JNIEnv *, jobject, jint _address)
{
    if( ioperm(_address, 1, 1) )
      return 0;
    else
      return inb(_address);
}


JNIEXPORT void JNICALL Java_promauto_jroboplc_plugin_system_SystemPlugin_writeHardwarePortNative
  (JNIEnv *, jobject, jint _address, jint _value)
{
    if( ioperm(_address, 1, 1) == 0 )
      outb(_value, _address);
}




JNIEXPORT void JNICALL Java_promauto_jroboplc_plugin_system_SystemPlugin_shutdownHardwareNative
  (JNIEnv *, jobject)
{

}
